/** 传入formList,动态追加组件 */
function dynamicComponent(formList) {
    let html = ``;
    formList.forEach((form) => {
        if (form.data) {
            html += `<div class="login-input ui-select">
  <label>${form.label}</label>
  <select id="${form.name}" name="${form.name}">
`;
            form.data.forEach((item) => {
                html += `  <option value="${item.id}">${item.name}</option>\n`;
            });
            html += `</select>
</div>
`;
        } else {
            html += `<div class="login-input">
  <label>${form.label}</label>
  <input type="${form.type}" class="list-input" id="${form.name}" name="${form.name}">
</div>
`;
        }
    });
    return html;
}

/** 表单的公共事件,如关闭、拖拽表单等 */
function dynamicComponentCommonEvent() {
    //获取关闭，隐藏登录框和遮挡层
    my$('#closeBtn').onclick = function () {
        my$('#login').style.display = 'none';
        my$('#bg').style.display = 'none';
        // 清空输入框
        my$('#login-input-content').innerHTML = '';
    };

    // 按下鼠标，移动登录框
    my$('#title').onmousedown = function (e) {
        // 获取此时可视区域的x-此时登录框距离左侧页面的横坐标
        let spaceX = e.clientX - my$('#login').offsetLeft;
        let spaceY = e.clientY - my$('#login').offsetTop;
        // 移动事件
        document.onmousemove = function (e) {
            // 新的可视区域的x-spaceX   == 登录框的left属性
            var x = e.clientX - spaceX + 250;
            var y = e.clientY - spaceY - 140;
            my$('#login').style.left = x + 'px';
            my$('#login').style.top = y + 'px';
        }
    };

    // 任何位置鼠标松开
    document.onmouseup = function (e) {
        document.onmousemove = null;
    }
}