/** 根据id、class、tag等获取dom元素,类似于jquery的$(选择器) */
function my$(selector) {
    let select = selector.substr(1);
    if (selector[0] === '#') {
        return document.getElementById(select);
    }
    return document.querySelector(selector);
}

/** 解析yyyy-MM-ddTHH:mm为yyyy-MM-dd HH:mm:ss */
function parseDateTimeLocal(dateTimeLocal) {
    if (dateTimeLocal == null) {
        return null;
    }
    const splits = dateTimeLocal.split('T');
    return splits[0] + ' ' + splits[1] + ':00';
}