package com.qst.iteam.filter;

import cn.hutool.json.JSONUtil;
import com.qst.iteam.utils.BaseContext;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.R;
import com.qst.iteam.utils.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.*;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


@WebFilter("/*")
public class LoginFilter implements Filter {
    // 路径匹配器
    private static final AntPathMatcher ANT_PATH_MATCHER = new AntPathMatcher();
    // 配置不拦截的路径
    private final String[] uriPassed = getUriPassed();


    private static String[] getUriPassed() {
        List<String> uriPassed = new ArrayList<>(Arrays.asList(
                // static
                "/backend/css/**",
                "/backend/images/**",
                "/backend/js/**",
                "/backend/src/**",
                "/backend/login.html",
////                 login
//                "/admin/login",
//                "/admin/logout",
                // Swagger
                "/doc.html",
                "/swagger-resources/**",
                "/webjars/**",
                "/v2/**",
                "/swagger-ui.html/**",
                "/swagger-ui.html"
        ));
        List<String> controllPaths = getControllerPaths();
        uriPassed.addAll(controllPaths);
        return uriPassed.toArray(new String[]{});
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // (1)获取请求的URL
        String requestUri = request.getRequestURI();
        // (2)判断本次请求是否需要处理, 如果不需要, 直接放行
        boolean checked = check(requestUri);
        if (checked) {
            chain.doFilter(request, response);
            return;
        }
        Long adminId = (Long) request.getSession().getAttribute(Constants.ADMIN_ID);
        if (adminId != null) {
            // 设置ThreadLocal
            BaseContext.setUserId(adminId);
            // 放行
            chain.doFilter(request, response);
            return;
        }
        if (ANT_PATH_MATCHER.match("/**", requestUri)) {
            response.sendRedirect("/backend/login.html");
            return;
        }
        // (4)如果未登录,返回未登录状态; 通过response流对象写json
        response.getWriter().write(JSONUtil.toJsonStr(R.error("NOTLOGIN")));
    }

    private boolean check(String uri) {
        for (String s : uriPassed) {
            if (ANT_PATH_MATCHER.match(s, uri)) {
                return true;
            }
        }
        return false;
    }

    private static List<String> getControllerPaths() {
        // 返回结果
        List<String> paths = new ArrayList<>();
        // 获取com.qst.iteam.controller包下的所有Class
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Controller.class));
        Set<BeanDefinition> candidates = scanner.findCandidateComponents("com.qst.iteam.controller");

        try {
            for (BeanDefinition candidate : candidates) {
                Class<?> controllerClass = Class.forName(candidate.getBeanClassName());
                // 获取访问path
                RequestMapping controllerRequestMapping = controllerClass.getAnnotation(RequestMapping.class);
                String controllerPath = controllerRequestMapping.value()[0];

                // 所有的Method
                for (Method method : controllerClass.getDeclaredMethods()) {
                    String path = null;
                    // 筛选出含有@XxxMapping的某个方法
                    GetMapping getMapping = method.getAnnotation(GetMapping.class);
                    PostMapping postMapping = method.getAnnotation(PostMapping.class);
                    PutMapping putMapping = method.getAnnotation(PutMapping.class);
                    DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
                    if (getMapping != null) {
                        path = StringUtils.getCorrectPath(getMapping.value()[0]);
                    } else if (postMapping != null) {
                        path = StringUtils.getCorrectPath(postMapping.value()[0]);
                    } else if (putMapping != null) {
                        path = StringUtils.getCorrectPath(putMapping.value()[0]);
                    } else if (deleteMapping != null) {
                        path = StringUtils.getCorrectPath(deleteMapping.value()[0]);
                    }
                    // 拼接完整path
                    if (path != null) {
                        String fullPath = controllerPath + path;
                        paths.add(fullPath);
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return paths;
    }
}
