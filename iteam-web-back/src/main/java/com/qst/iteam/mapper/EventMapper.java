package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Event;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EventMapper extends BaseMapper<Event> {
}
