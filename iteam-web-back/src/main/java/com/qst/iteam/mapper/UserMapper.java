package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
