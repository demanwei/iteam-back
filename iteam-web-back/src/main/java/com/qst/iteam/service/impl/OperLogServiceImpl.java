package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.OperLog;
import com.qst.iteam.mapper.OperLogMapper;
import com.qst.iteam.service.OperLogService;
import org.springframework.stereotype.Service;

@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements OperLogService {
}
