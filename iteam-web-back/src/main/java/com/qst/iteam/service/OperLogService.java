package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.OperLog;

public interface OperLogService extends IService<OperLog> {
}
