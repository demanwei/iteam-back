package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.EventComment;
import com.qst.iteam.dto.EventCommentDto;
import com.qst.iteam.mapper.EventCommentMapper;
import com.qst.iteam.service.EventCommentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventCommentServiceImpl extends ServiceImpl<EventCommentMapper, EventComment> implements EventCommentService {
    @Override
    public List<EventCommentDto> listByEventId(Long eventId) {
        return baseMapper.listByEventId(eventId);
    }
}
