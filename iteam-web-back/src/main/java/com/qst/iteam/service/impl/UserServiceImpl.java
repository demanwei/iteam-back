package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.User;
import com.qst.iteam.mapper.UserMapper;
import com.qst.iteam.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
