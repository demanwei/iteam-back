package com.qst.iteam.dto;

import com.qst.iteam.domain.EventComment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("活动评论(数据传输对象)")
public class WebEventCommentDto extends EventComment {
    @ApiModelProperty("那个活动,这个活动的name")
    private String eventName;
    @ApiModelProperty("哪个用户评论的,他的name")
    private String userName;
    @ApiModelProperty("哪个用户评论的,他的头像")
    private String headImg;
}
