package com.qst.iteam.utils;


/**
 * 基于ThreadLocal封装的工具类,包含get和set功能
 * 作用域是某一个线程之内
 **/
public class BaseContext {
    private static final ThreadLocal<Long> THREAD_LOCAL = new ThreadLocal<>();

    public static void setUserId(Long id) {
        THREAD_LOCAL.set(id);
    }

    public static Long getUserId() {
        return THREAD_LOCAL.get();
    }

    public static void removeUserId() {
        THREAD_LOCAL.remove();
    }
}
