package com.qst.iteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy // 开启AOP自动代理
@ServletComponentScan   // 扫描Filter,Interceptor等web组件
public class IteamWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(IteamWebApplication.class, args);
    }
}
