package com.qst.iteam.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Admin;
import com.qst.iteam.service.AdminService;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/admin")
@Api(tags = "管理员接口")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @GetMapping("/list")
    @ApiOperation("获取管理员列表")
    public R<List<Admin>> list() {
        List<Admin> adminList = adminService.list(
                new LambdaQueryWrapper<Admin>()
                        .eq(Admin::getEnd, Constants.END)
        );
        return R.success(adminList);
    }

    @GetMapping("/info")
    @ApiOperation("根据id获取管理员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminId", value = "管理员id", required = true, paramType = "query", example = "1")
    })
    public R<Admin> info(Long adminId) {
        if (adminId == null) {
            return R.error("请求字段缺失");
        }
        Admin admin = adminService.getById(adminId);
        return R.success(admin);
    }

    @PostMapping("/login")
    @ApiOperation("管理员登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "admin", value = "请求体,须包含username和password两个字段", required = true, paramType = "body",
                    example = "{\"username\":\"90217\", \"password\": \"123abc\"}")
    })
    public R<Admin> login(@RequestBody Admin admin, HttpSession session) {
        if (admin == null || StrUtil.isEmpty(admin.getUsername()) || StrUtil.isEmpty(admin.getPassword())) {
            return R.error("请求字段缺失");
        }
        // md5加密
        String passwordMd5 = DigestUtils.md5DigestAsHex(admin.getPassword().getBytes());
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<Admin>()
                .eq(Admin::getUsername, admin.getUsername())
                .eq(Admin::getPassword, passwordMd5)
                .eq(Admin::getEnd, Constants.END);
        // 查找
        Admin targetAdmin = adminService.getOne(queryWrapper);
        if (targetAdmin == null) {
            return R.error("账号或密码错误");
        }
        // 添加session
        session.setAttribute(Constants.ADMIN_ID, targetAdmin.getId());
        return R.success(targetAdmin);
    }


    @PostMapping("/logout")
    @ApiOperation("管理员退出登录")
    public R<Admin> logout(HttpSession session) {
        session.removeAttribute(Constants.ADMIN_ID);
        return R.success();
    }


    @PostMapping("/insert")
    @ApiOperation("新增管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "admin", value = "管理员实体,至少包含username和password两个字段", required = true, paramType = "body",
                    example = "{\"username\":\"test111\", \"password\": \"123abc\"}")
    })
    public R<Admin> insert(@RequestBody Admin admin) {
        if (admin == null || StrUtil.isEmpty(admin.getUsername()) || StrUtil.isEmpty(admin.getPassword())) {
            return R.error("请求字段缺失");
        }

        // username是否已注册
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<Admin>()
                .eq(Admin::getUsername, admin.getUsername());
        // 查找
        Admin targetAdmin = adminService.getOne(queryWrapper);
        if (targetAdmin != null) {
            return R.error("该账号已注册");
        }
        // 插入
        String passwordMd5 = DigestUtils.md5DigestAsHex(admin.getPassword().getBytes());
        admin.setPassword(passwordMd5);
        // 设置操作端
        admin.setEnd(Constants.END);
        boolean success = adminService.save(admin);
        return success ? R.success(admin) : R.error();
    }
}
