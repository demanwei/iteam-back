package com.qst.iteam.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qst.iteam.domain.Event;
import com.qst.iteam.domain.EventComment;
import com.qst.iteam.domain.User;
import com.qst.iteam.dto.EventCommentDto;
import com.qst.iteam.dto.WebEventCommentDto;
import com.qst.iteam.service.EventCommentService;
import com.qst.iteam.service.EventService;
import com.qst.iteam.service.UserService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/eventComment")
@Api(tags = "活动评论相关接口")
public class EventCommentController {
    @Autowired
    private EventCommentService eventCommentService;
    @Autowired
    private UserService userService;
    @Autowired
    private EventService eventService;

    @GetMapping("/listPage")
    @ApiOperation("活动评论列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页码", defaultValue = "1", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "size", value = "每页条数", defaultValue = "20", required = false, paramType = "query", example = "20"),
            @ApiImplicitParam(name = "kw", value = "搜索关键字", required = false, paramType = "query", example = "骑行"),
    })
    public R<IPage<EventComment>> listPage(Integer current, Integer size, String kw) {
        if (current == null || current <= 0) {
            current = 1;
        }
        if (size == null || size <= 0) {
            size = 20;
        }
        IPage<EventComment> iPage = new Page<>(current, size);
        LambdaQueryWrapper<EventComment> wrapper = new LambdaQueryWrapper<EventComment>()
                .orderByDesc(EventComment::getCreateTime);
        // 模糊查询,根据活动名称
        if (kw != null) {
            List<Long> eventIdList = eventService.list(
                    new LambdaQueryWrapper<Event>()
                            .like(Event::getName, kw)
            ).stream()
                    .map(Event::getId)
                    .collect(Collectors.toList());
            // eventIdList为空列表,会出错,因此如果没查到eventIdList,故意查一条不存在的数据-999
            if (eventIdList.size() > 0) {
                wrapper.in(EventComment::getEventId, eventIdList);
            } else {
                wrapper.eq(EventComment::getEventId, -999L);
            }
//            wrapper.in(EventComment::getEventId, eventIdList.size() > 0 ? eventIdList : new int[]{-999});
        }
        eventCommentService.page(iPage, wrapper);
        // 让每个评论都携带用户名、头像
        List<EventComment> webEventCommentDtoList = iPage.getRecords().stream()
                .map((eventComment) -> {
                    WebEventCommentDto webEventCommentDto = new WebEventCommentDto();
                    BeanUtil.copyProperties(eventComment, webEventCommentDto);
                    // 设置活动信息
                    Event event = eventService.getById(eventComment.getEventId());
                    if (event != null) {
                        webEventCommentDto.setEventName(event.getName());
                    }
                    // 设置用户信息
                    User user = userService.getById(eventComment.getUserId());
                    if (user != null) {
                        webEventCommentDto.setUserName(user.getName());
                        webEventCommentDto.setHeadImg(user.getHeadImg());
                    }
                    return webEventCommentDto;
                }).collect(Collectors.toList());
        iPage.setRecords(webEventCommentDtoList);
        return R.success(iPage);
    }

    @GetMapping("/listByEventId")
    @ApiOperation("根据eventId查找所有相关评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query")
    })
    public R<List<EventCommentDto>> listByEventId(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        // 查询列表
        List<EventCommentDto> eventCommentDtoList = eventCommentService.listByEventId(eventId);
        return R.success(eventCommentDtoList);
    }


    @PostMapping("/insert")
    @ApiOperation("新增评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventComment", value = "评论实体,须包含eventId,userId,content字段", required = true, paramType = "body")
    })
    public R<Object> insert(@RequestBody EventComment eventComment) {
        if (eventComment == null || eventComment.getEventId() == null
                || eventComment.getUserId() == null || eventComment.getContent() == null) {
            return R.error("请求字段缺失");
        }
        // 新增实体
        boolean success = eventCommentService.save(eventComment);
        return success ? R.success() : R.error();
    }


    @DeleteMapping("/delete")
    @ApiOperation("根据id删除某条评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "评论id", required = true, paramType = "query")
    })
    public R<Object> deleteById(Long id) {
        if (id == null) {
            return R.error("请求字段缺失");
        }
        boolean success = eventCommentService.removeById(id);
        return success ? R.success() : R.error();
    }

    @DeleteMapping("/deleteByEventId")
    @ApiOperation("根据eventId删除所有相关评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query")
    })
    public R<Object> deleteByEventId(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        boolean success = eventCommentService.remove(new LambdaQueryWrapper<EventComment>()
                .eq(EventComment::getEventId, eventId));
        return success ? R.success() : R.error();
    }
}
