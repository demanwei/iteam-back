package com.qst.iteam.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qst.iteam.domain.Event;
import com.qst.iteam.service.EventService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/event")
@Api(tags = "活动相关接口")
public class EventController {
    @Autowired
    private EventService eventService;

    @GetMapping("/listPage")
    @ApiOperation("活动列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页码", defaultValue = "1", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "size", value = "每页条数", defaultValue = "20", required = false, paramType = "query", example = "20"),
            @ApiImplicitParam(name = "kw", value = "搜索关键字", required = false, paramType = "query", example = "草原"),
    })
    public R<IPage<Event>> listPage(Integer current, Integer size, String kw) {
        if (current == null || current <= 0) {
            current = 1;
        }
        if (size == null || size <= 0) {
            size = 20;
        }
        IPage<Event> iPage = new Page<>(current, size);
        LambdaQueryWrapper<Event> wrapper = new LambdaQueryWrapper<Event>()
                .orderByDesc(Event::getCreateTime);
        // 模糊查询
        if (kw != null) {
            wrapper.like(Event::getName, kw)
                    .or()
                    .like(Event::getIntro, kw);
        }
        eventService.page(iPage, wrapper);
        return R.success(iPage);
    }

    @GetMapping("/info")
    @ApiOperation("根据eventId获取某个活动的信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query")
    })
    public R<Event> info(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        Event event = eventService.getById(eventId);
        if (event == null) {
            return R.error("未找到该活动");
        }
        return R.success(event);
    }


    @PostMapping("/insert")
    @ApiOperation("新增活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "event", value = "活动实体", required = true, paramType = "body")
    })
    public R<Event> insert(@RequestBody Event event) {
        if (event == null) {
            return R.error("请求字段缺失");
        }
        // 插入表
        boolean success = eventService.save(event);
        return success ? R.success(event) : R.error();
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据eventId删除活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query"),
    })
    public R<Object> delete(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        // 删除event表的记录
        boolean success = eventService.removeById(eventId);
        return success ? R.success() : R.error();
    }


    @PutMapping("/update")
    @ApiOperation("修改活动信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "event", value = "活动实体,至少包含id和一个要修改的字段", required = true, paramType = "body")
    })
    public R<Event> update(@RequestBody Event event) {
        if (event == null || event.getId() == null) {
            return R.error("请求字段缺失");
        }
        // 修改
        boolean success = eventService.updateById(event);
        return success ? R.success() : R.error();
    }
}
