package com.qst.iteam.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qst.iteam.domain.User;
import com.qst.iteam.service.UserService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "用户相关接口")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    @ApiOperation("获取用户列表")
    public R<List<User>> list() {
        List<User> userList = userService.list();
        return R.success(userList);
    }

    @GetMapping("/listPage")
    @ApiOperation("用户列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页码", defaultValue = "1", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "size", value = "每页条数", defaultValue = "20", required = false, paramType = "query", example = "20"),
            @ApiImplicitParam(name = "kw", value = "搜索关键字", required = false, paramType = "query", example = "青春"),
    })
    public R<IPage<User>> listPage(Integer current, Integer size, String kw) {
        if (current == null || current <= 0) {
            current = 1;
        }
        if (size == null || size <= 0) {
            size = 20;
        }
        IPage<User> iPage = new Page<>(current, size);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<User>()
                .orderByDesc(User::getCreateTime);
        // 模糊查询
        if (kw != null) {
            wrapper.like(User::getUsername, kw)
                    .or()
                    .like(User::getName, kw);
        }
        userService.page(iPage, wrapper);
        return R.success(iPage);
    }


    @GetMapping("/info")
    @ApiOperation("根据userId查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query",
                    example = "1")
    })
    public R<Object> info(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // 查找用户
        User user = userService.getById(userId);
        if (user == null) {
            return R.error("未找到该用户");
        }
        return R.success(user);
    }

    @PostMapping("/insert")
    @ApiOperation("添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户实体,至少包含username和password两个字段", required = true, paramType = "body",
                    example = "{\"username\":\"aaaaaa\", \"password\": \"aaaaaa\"}")
    })
    public R<User> insert(@RequestBody User user) {
        if (user == null || StrUtil.isEmpty(user.getUsername()) || StrUtil.isEmpty(user.getPassword())) {
            return R.error("请求字段缺失");
        }

        // 查找
        User targetUser = userService.getOne(new LambdaQueryWrapper<User>()
                .eq(User::getUsername, user.getUsername()));
        if (targetUser != null) {
            return R.error("添加失败，用户名已经存在");
        }
        // 插入
        boolean success = userService.save(user);
        return success ? R.success(user) : R.error();
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据userId删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1")
    })
    public R<Object> delete(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // 查找用户
        boolean success = userService.removeById(userId);
        return success ? R.success() : R.error();
    }


    @PutMapping("/update")
    @ApiOperation("更新用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户实体,须包含用户id和要更新的字段", required = true, paramType = "body",
                    example = "{\"id\": 2, \"username\":\"aaabbb\", \"password\": \"aaabbb\"}")
    })
    public R<Object> update(@RequestBody User user) {
        if (user == null || user.getId() == null) {
            return R.error("请求字段缺失");
        }
        // 根据id更新用户
        boolean success = userService.updateById(user);
        return success ? R.success() : R.error();
    }
}
