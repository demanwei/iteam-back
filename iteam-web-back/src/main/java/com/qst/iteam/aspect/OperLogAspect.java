package com.qst.iteam.aspect;

import com.qst.iteam.domain.OperLog;
import com.qst.iteam.service.OperLogService;
import com.qst.iteam.utils.BaseContext;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.ReflectUtils;
import com.qst.iteam.utils.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Component
@Aspect
public class OperLogAspect {
    @Autowired
    private OperLogService operLogService;

    @After("execution(* com.qst.iteam.controller.AdminController.login(..))")
    public void afterAdminLogin(JoinPoint joinPoint) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        Parameter[] parameters = method.getParameters();
        Object[] args = joinPoint.getArgs();
        // 从session中获取adminId
        for (int i = 0; i < parameters.length; i++) {
            // 参数metadata
            String parameterName = parameters[i].getName();
            if ("session".equals(parameterName)) {
                HttpSession session = (HttpSession) args[i];
                Object adminIdObj = session.getAttribute(Constants.ADMIN_ID);
                if (adminIdObj != null) {
                    Long adminId = (long) adminIdObj;
                    // 保存操作日志
                    saveOperLog(joinPoint, method, adminId, "/admin/login");   // 注意path的更新
                }
                break;
            }
        }
    }

    @Before("execution(* com.qst.iteam.controller.AdminController.logout(..))")
    public void beforeAdminLogout(JoinPoint joinPoint) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        Parameter[] parameters = method.getParameters();
        Object[] args = joinPoint.getArgs();
        // 从session中获取adminId
        for (int i = 0; i < parameters.length; i++) {
            // 参数metadata
            String parameterName = parameters[i].getName();
            if ("session".equals(parameterName)) {
                HttpSession session = (HttpSession) args[i];
                Object adminIdObj = session.getAttribute(Constants.ADMIN_ID);
                if (adminIdObj != null) {
                    Long adminId = (long) adminIdObj;
                    // 保存操作日志
                    saveOperLog(joinPoint, method, adminId, "/admin/logout");   // 注意path的更新
                }
                break;
            }
        }
    }

    // AOP切面表达式,要去掉管理员登录和注销,因为获取adminId的行为比较复杂,故使用上面两个单独的AOP
    @Before("execution(* com.qst.iteam.controller.*.*(..)) && " +
            "!execution(* com.qst.iteam.controller.AdminController.login(..)) && " +
            "!execution(* com.qst.iteam.controller.AdminController.logout(..))")
    public void before(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();

        // 访问路径: controller的path + method的path
        String path = null;
        Method method = ((MethodSignature) signature).getMethod();
        Class<?> controllerClass = method.getDeclaringClass();
        RequestMapping controllerRequestMapping = controllerClass.getAnnotation(RequestMapping.class);
        String controllerPath = controllerRequestMapping.value()[0];
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (postMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(postMapping.value()[0]);
        } else if (putMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(putMapping.value()[0]);
        } else if (deleteMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(deleteMapping.value()[0]);
        }
        // 保存操作日志
        Long runnerId = BaseContext.getUserId();
        if (path != null) {
            saveOperLog(joinPoint, method, runnerId, path);
        }
    }


    /**
     * 对于非查询操作, 记录操作日志
     */
    private void saveOperLog(JoinPoint joinPoint, Method method, Long runnerId, String path) {
        OperLog operLog = new OperLog();
        // 获取操作者的id
        operLog.setRunnerId(runnerId);
        // 操作者类型
        operLog.setRunnerType(Constants.END);
        // 访问path
        operLog.setPath(path);

        // 获取请求参数
        Object[] args = joinPoint.getArgs();
        Parameter[] parameters = method.getParameters();
        StringBuilder param = new StringBuilder();
        for (int i = 0; i < parameters.length; i++) {
            // 参数metadata
            String parameterName = parameters[i].getName();
            // 参数值
            Object parameterValue = args[i];
            if (ReflectUtils.isHttp(parameterValue)) {
                // 如果是http参数,如session,request,response,直接跳过
                continue;
            } else if (ReflectUtils.isPrimitive(parameterValue)) {
                // 基本类型 => userId=1,
                param.append(parameterName)
                        .append("=")
                        .append(parameterValue)
                        .append(",");
            } else if (parameterValue != null) {    // 调用时的参数可能没传,用的默认值
                // [Rule(id=null, metricName=cpu_usage, metricValue=10, metricUnit=percent, userId=1, createTime=null, updateTime=null)]
                // [User(id=null, username=90217, password=123abc, createTime=null)]
                // 引用类型 => username=90217, password=123abc,
                for (Field field : parameterValue.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        Object value = field.get(parameterValue);
                        if (value != null) {
                            param.append(field.getName())
                                    .append("=")
                                    .append(value)
                                    .append(",");
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // 没有参数,不设置,直接为null
        if (param.length() > 1) {
            // 去掉最后一个逗号","
            param.deleteCharAt(param.length() - 1);
            operLog.setParam(param.toString());
        }

        // 保存至数据库
        operLogService.save(operLog);
    }
}
