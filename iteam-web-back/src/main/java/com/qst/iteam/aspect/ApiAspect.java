package com.qst.iteam.aspect;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Api;
import com.qst.iteam.service.ApiService;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

@Component
@Aspect
public class ApiAspect {
    @Autowired
    private ApiService apiService;

    @Before("execution(* com.qst.iteam.controller.*.*(..))")
    public void before(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();

        // 访问路径: controller的path + method的path
        String path = null;
        Method method = ((MethodSignature) signature).getMethod();
        Class<?> controllerClass = method.getDeclaringClass();
        RequestMapping controllerRequestMapping = controllerClass.getAnnotation(RequestMapping.class);
        String controllerPath = controllerRequestMapping.value()[0];
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (getMapping != null) {
            // 最终 访问路径
            path = controllerPath + StringUtils.getCorrectPath(getMapping.value()[0]);
        } else if (postMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(postMapping.value()[0]);
        } else if (putMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(putMapping.value()[0]);
        } else if (deleteMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(deleteMapping.value()[0]);
        }

        // 增加visit访问数
        if (path != null) {
            changeVisit(path);
        }
    }


    /**
     * 增加visit访问数
     */
    private void changeVisit(String path) {
        // 根据path查询api
        Api api = apiService.getOne(
                new LambdaQueryWrapper<Api>()
                        .eq(Api::getEnd, Constants.END)
                        .eq(Api::getPath, path)
        );
        if (api != null) {
            // 增加visit访问数
            api.setVisit(api.getVisit() + 1);
            // 更新
            apiService.updateById(api);
        }
    }
}
