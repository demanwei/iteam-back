package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Alert;

public interface AlertService extends IService<Alert> {
}
