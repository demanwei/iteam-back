package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Rule;

public interface RuleService extends IService<Rule> {
}
