package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Performance;

public interface PerformanceService extends IService<Performance> {
}
