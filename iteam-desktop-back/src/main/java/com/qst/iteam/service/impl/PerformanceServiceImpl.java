package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.Performance;
import com.qst.iteam.mapper.PerformanceMapper;
import com.qst.iteam.service.PerformanceService;
import org.springframework.stereotype.Service;

@Service
public class PerformanceServiceImpl extends ServiceImpl<PerformanceMapper, Performance> implements PerformanceService {
}
