package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Admin;

public interface AdminService extends IService<Admin> {
}
