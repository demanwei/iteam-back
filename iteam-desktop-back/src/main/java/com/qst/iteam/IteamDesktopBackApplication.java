package com.qst.iteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling   // 开启定时任务
@EnableAspectJAutoProxy // 开启AOP自动代理
public class IteamDesktopBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(IteamDesktopBackApplication.class, args);
    }
}
