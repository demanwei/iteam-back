package com.qst.iteam.utils;

import com.qst.iteam.schedule.PerformanceSchedule;
import org.hyperic.sigar.Sigar;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Component
public class SigarUtils {
    public static void loadSigarLibrary() {
        try {
            // 获取Sigar库文件的输入流
            InputStream inputStream = PerformanceSchedule.class.getResourceAsStream("/lib/sigar-amd64-winnt.dll");

            // 创建临时文件
            File tempFile = File.createTempFile("sigar-amd64-winnt", ".dll");

            // 将输入流中的数据写入临时文件
            try (FileOutputStream outputStream = new FileOutputStream(tempFile)) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }

            // 设置临时文件的路径
            System.setProperty("org.hyperic.sigar.path", tempFile.getParent());

            // 加载Sigar库
            System.load(tempFile.getAbsolutePath());

            // 删除临时文件
            Files.deleteIfExists(tempFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Bean
    public Sigar sigar() {
        loadSigarLibrary();
        return new Sigar();
    }
}
