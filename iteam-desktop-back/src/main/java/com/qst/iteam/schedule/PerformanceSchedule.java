package com.qst.iteam.schedule;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Admin;
import com.qst.iteam.domain.Alert;
import com.qst.iteam.domain.Performance;
import com.qst.iteam.domain.Rule;
import com.qst.iteam.service.AdminService;
import com.qst.iteam.service.AlertService;
import com.qst.iteam.service.PerformanceService;
import com.qst.iteam.service.RuleService;
import com.qst.iteam.utils.DateTimeUtils;
import com.qst.iteam.utils.MailUtils;
import com.qst.iteam.utils.NumberUtils;
import org.hyperic.sigar.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.management.MemoryUsage;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PerformanceSchedule {
    @Value("${spring.application.name}")
    private String appName;

    @Resource(name = "heapMemoryUsage")
    private MemoryUsage heapMemoryUsage;
    @Resource(name = "nonHeapMemoryUsage")
    private MemoryUsage nonHeapMemoryUsage;
    @Autowired
    private Sigar sigar;
    @Autowired
    private PerformanceService performanceService;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private AlertService alertService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private MailUtils mailUtils;


    @Scheduled(cron = "*/10 * * * * ?")
    public void monitor() {
        // 获取所有rule指标列表
        Map<String, Rule> metricMap = ruleService.list().stream()
                .collect(Collectors.toMap(Rule::getMetricName, rule -> rule));

        try {
            Performance performance = new Performance();
            List<Alert> alertList = new ArrayList<>();

            // CPU使用率
            monitorCpu(performance, metricMap, alertList);
            // 内存使用情况
            monitorMem(performance, metricMap, alertList);
            // 磁盘使用情况
            monitorDisk(performance, metricMap, alertList);
            // 网络流量
            monitorNet(performance, metricMap, alertList);
            // heap
            monitorHeap(performance, metricMap, alertList);
            // non_heap
            monitorNonHeap(performance, metricMap, alertList);

            // 监控信息写入DB
            performanceService.save(performance);

            // 是否预警
            if (alertList.size() > 0) {
                // 保存预警信息
                alertService.saveBatch(alertList);
                // 构造消息,发邮件
                /*
                <table>
                    <caption>[{0}]发生警报,消息信息如下:</caption>
                    <thead>
                        <tr>
                            <th>指标名称</th>
                            <th>指标单位</th>
                            <th>指标真实值</th>
                            <th>规则阈值</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                            <td>{3}</td>
                        </tr>
                    </tbody>
                </table>
                */
                StringBuilder html = new StringBuilder(
                        "<body>\n" +
                                "<table border=" + "'1'" + ">\n" +
                                "  <caption style=" + "'color:red;'" + ">[" + DateTimeUtils.now() + "]发生警报,消息信息如下:</caption>\n" +
                                "  <thead>\n" +
                                "    <tr>\n" +
                                "      <th>指标名称</th>\n" +
                                "      <th>指标单位</th>\n" +
                                "      <th>指标真实值</th>\n" +
                                "      <th>规则阈值</th>\n" +
                                "    </tr>\n" +
                                "  </thead>\n" +
                                "  <tbody>");
                alertList.forEach((alert) -> {
                    html.append(MessageFormat.format(
                            "<tr>\n" +
                                    "  <td>{0}</td>\n" +
                                    "  <td>{1}</td>\n" +
                                    "  <td>{2}</td>\n" +
                                    "  <td>{3}</td>\n" +
                                    "</tr>", alert.getMetricName(), alert.getMetricUnit()
                            , alert.getMetricValueReal(), alert.getMetricValueRule()));
                });
                html.append("  </tbody>\n" +
                        "</table>\n" +
                        "</body>");
                sendEmailToAdmins(html.toString());
            }
        } catch (SigarException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 监控Cpu使用率
     */
    private void monitorCpu(Performance performance, Map<String, Rule> metricMap,
                            List<Alert> alertList) throws SigarException {
        double cpuUsage = NumberUtils.round(sigar.getCpuPerc().getCombined() * 100, 2);
        performance.setCpuUsage(cpuUsage);
        performance.setCpuUnit("percent");
        checkAndGenerateAlert(new String[]{"cpu_usage"}, metricMap, cpuUsage, null, alertList);
    }

    /**
     * 监控内存使用情况
     */
    private void monitorMem(Performance performance, Map<String, Rule> metricMap,
                            List<Alert> alertList) throws SigarException {
        Mem mem = sigar.getMem();
        performance.setMemoryTotal(NumberUtils.round((double) mem.getTotal() / 1024 / 1024 / 1024 * 1.0D, 2));
        double used = NumberUtils.round((double) mem.getUsed() / 1024 / 1024 / 1024, 2);
        performance.setMemoryUsed(used);
        performance.setMemoryUnit("GB");
        // 如果是percent阈值
        double memeoryUsage = NumberUtils.round((double) mem.getUsed() / mem.getTotal() * 100, 2);
        checkAndGenerateAlert(new String[]{"memory_usage", "memory_used"}, metricMap, memeoryUsage, used, alertList);
    }

    /**
     * 监控磁盘使用情况
     */
    private void monitorDisk(Performance performance, Map<String, Rule> metricMap,
                             List<Alert> alertList) throws SigarException {
        FileSystem[] fslist = sigar.getFileSystemList();
        long diskTotalBytes = 0;
        long diskUsedBytes = 0;
        for (FileSystem fs : fslist) {
            FileSystemUsage usage = sigar.getFileSystemUsage(fs.getDirName());
            diskTotalBytes += usage.getTotal();
            diskUsedBytes += usage.getUsed();
        }
        performance.setDiskTotal(NumberUtils.round((double) diskTotalBytes / 1024 / 1024, 2));
        double used = NumberUtils.round((double) diskUsedBytes / 1024 / 1024, 2);
        performance.setDiskUsed(used);
        performance.setDiskUnit("GB");
        double diskUsage = NumberUtils.round((double) diskUsedBytes / diskTotalBytes * 100, 2);
        checkAndGenerateAlert(new String[]{"disk_usage", "disk_used"}, metricMap, diskUsage, used, alertList);
    }

    /**
     * 监控网络流量情况
     */
    private void monitorNet(Performance performance, Map<String, Rule> metricMap,
                            List<Alert> alertList) throws SigarException, InterruptedException {
        long totalRxBytesStart = 0;
        long totalTxBytesStart = 0;
        long start = System.currentTimeMillis();
        String[] ifNames = sigar.getNetInterfaceList();
        for (String ifName : ifNames) {
            NetInterfaceStat statStart = sigar.getNetInterfaceStat(ifName);
            totalRxBytesStart += statStart.getRxBytes();
            totalTxBytesStart += statStart.getTxBytes();
        }

        Thread.sleep(2000);

        long totalRxBytesEnd = 0;
        long totalTxBytesEnd = 0;
        long end = System.currentTimeMillis();
        ifNames = sigar.getNetInterfaceList();
        for (String ifName : ifNames) {
            NetInterfaceStat statStart = sigar.getNetInterfaceStat(ifName);
            totalRxBytesEnd += statStart.getRxBytes();
            totalTxBytesEnd += statStart.getTxBytes();
        }
        long totalRxBytes = (totalRxBytesEnd - totalRxBytesStart) / (end - start) * 1000;
        long totalTxBytes = (totalTxBytesEnd - totalTxBytesStart) / (end - start) * 1000;
        double netRx = NumberUtils.round((double) totalRxBytes / 1024 / 1024, 2);
        double netTx = NumberUtils.round((double) totalTxBytes / 1024 / 1024, 2);
        performance.setNetRx(netRx);
        performance.setNetTx(netTx);
        performance.setNetUnit("MBps");
        checkAndGenerateAlert(new String[]{"net"}, metricMap, null, netRx, alertList);
        checkAndGenerateAlert(new String[]{"net"}, metricMap, null, netTx, alertList);
    }

    /**
     * 监控jvm堆内存
     */
    private void monitorHeap(Performance performance, Map<String, Rule> metricMap,
                             List<Alert> alertList) throws SigarException {
        double used = NumberUtils.round((double) heapMemoryUsage.getUsed() / 1024 / 1024, 2);
        performance.setHeapUsed(used);
        performance.setHeapMax(NumberUtils.round((double) heapMemoryUsage.getMax() / 1024 / 1024, 2));
        performance.setHeapUnit("MB");
        double heapUsage = NumberUtils.round((double) heapMemoryUsage.getUsed() / heapMemoryUsage.getMax() * 100, 2);
        checkAndGenerateAlert(new String[]{"heap_usage", "heap_used"}, metricMap, heapUsage, used, alertList);
    }

    /**
     * 监控jvm非堆内存
     */
    private void monitorNonHeap(Performance performance, Map<String, Rule> metricMap,
                                List<Alert> alertList) throws SigarException {
        double used = NumberUtils.round((double) nonHeapMemoryUsage.getUsed() / 1024 / 1024, 2);
        performance.setNonHeapUsed(used);
        performance.setNonHeapMax(NumberUtils.round((double) nonHeapMemoryUsage.getMax() / 1024 / 1024, 2));
        performance.setNonHeapUnit("MB");
        double nonHeapUsage = NumberUtils.round((double) nonHeapMemoryUsage.getUsed() / nonHeapMemoryUsage.getMax() * 100, 2);
        checkAndGenerateAlert(new String[]{"non_heap_usage", "non_heap_used"}, metricMap, nonHeapUsage, used, alertList);
    }

    /**
     * 如果指标真实值 > 规则阈值,则生成预警
     * 发送邮件
     */
    private void checkAndGenerateAlert(String[] keys, Map<String, Rule> metricMap, Double metricValueReal,
                                       Double used, List<Alert> alertList) {
        for (String key : keys) {
            if (key != null && metricMap.containsKey(key)) {
                // 如果存在指标,才检测
                Rule rule = metricMap.get(key);
                if ("percent".equals(rule.getMetricUnit())) {
                    if (metricValueReal > rule.getMetricValue()) {
                        // 预警
                        Alert alert = new Alert();
                        alert.setMetricName(key);
                        alert.setMetricValueReal(metricValueReal);
                        alert.setMetricValueRule(rule.getMetricValue());
                        alert.setMetricUnit(rule.getMetricUnit());
                        alertList.add(alert);
                    }
                } else {
                    if (used > rule.getMetricValue()) {
                        // 预警
                        Alert alert = new Alert();
                        alert.setMetricName(key);
                        alert.setMetricValueReal(used);
                        alert.setMetricValueRule(rule.getMetricValue());
                        alert.setMetricUnit(rule.getMetricUnit());
                        alertList.add(alert);
                    }
                }
            }
        }
    }

    /**
     * 发送邮件
     */
    private void sendEmailToAdmins(String text) {
        // 获取开启邮件接收的用户
        List<Admin> adminList = adminService.list(
                new LambdaQueryWrapper<Admin>()
                        .eq(Admin::getReceiveEmail, 1)
        );
        // 发邮件
        final String title = appName + "-系统警报提醒";
        adminList.forEach((user) -> mailUtils.sendHtmlEmail(user.getEmail(), title, text));
    }
}
