package com.qst.iteam.schedule;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.utils.MailUtils;
import com.qst.iteam.domain.Admin;
import com.qst.iteam.domain.Alert;
import com.qst.iteam.service.AdminService;
import com.qst.iteam.service.AlertService;
import com.qst.iteam.utils.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class DbSchedule {
    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AlertService alertService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private MailUtils mailUtils;

    // 每分钟执行一次
    @Scheduled(cron = "*/20 * * * * ?")
    public void pingPong() {
        Integer result = jdbcTemplate.queryForObject("SELECT 1", Integer.class);
        boolean pong = result != null && result == 1;
        if (!pong) {
            log.error("[{}] MySQL is not responding..", DateTimeUtils.now());
            // 生成预警
            Alert alert = new Alert();
            alert.setMetricName("db_connect");
            alert.setMetricValueReal(0D);
            alert.setMetricValueReal(1D);
            alert.setMetricUnit("boolean");
            alertService.save(alert);
            // 发送邮件
            // 获取开启邮件接收的用户
            List<Admin> adminList = adminService.list(
                    new LambdaQueryWrapper<Admin>()
                            .eq(Admin::getReceiveEmail, 1)
            );
            // 发邮件
            final String text = "[" + DateTimeUtils.now() + "]数据库连接失败...";
            final String title = appName + "-系统警报提醒";
            adminList.forEach((user) -> {
                // 每个开启接收的用户都发
                String to = user.getEmail();
                mailUtils.sendEmail(to, title, text);
            });
        }
    }
}
