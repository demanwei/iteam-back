package com.qst.iteam.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qst.iteam.domain.OperLog;
import com.qst.iteam.service.OperLogService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operLog")
@Api(tags = "操作日志管理接口")
public class OperLogController {
    @Autowired
    private OperLogService operLogService;

    @GetMapping("/byAdmin")
    @ApiOperation("根据adminId分页查询操作日志列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "adminId", value = "管理员id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "current", value = "当前页码", defaultValue = "1", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "size", value = "每页条数", defaultValue = "20", required = false, paramType = "query", example = "20")
    })
    public R<IPage<OperLog>> byAdmin(Long adminId, Integer current, Integer size) {
        if (adminId == null) {
            return R.error("请求字段缺失");
        }
        if (current == null || current <= 0) {
            current = 1;
        }
        if (size == null || size <= 0) {
            size = 20;
        }
        IPage<OperLog> iPage = new Page<>(current, size);
        Wrapper<OperLog> wrapper = new LambdaQueryWrapper<OperLog>()
                .eq(OperLog::getRunnerId, adminId)
                .orderByDesc(OperLog::getCreateTime);
        operLogService.page(iPage, wrapper);
        return R.success(iPage);
    }


    @GetMapping("/byRunnerType")
    @ApiOperation("根据操作者类型分页查询操作日志列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "runnerType", value = "操作者类型", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "current", value = "当前页码", defaultValue = "1", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "size", value = "每页条数", defaultValue = "20", required = false, paramType = "query", example = "20")
    })
    public R<IPage<OperLog>> listByRunnerType(String runnerType, Integer current, Integer size) {
        if (runnerType == null) {
            return R.error("请求字段缺失");
        }
        if (current == null || current <= 0) {
            current = 1;
        }
        if (size == null || size <= 0) {
            size = 20;
        }
        IPage<OperLog> iPage = new Page<>(current, size);
        Wrapper<OperLog> wrapper = new LambdaQueryWrapper<OperLog>()
                .eq(OperLog::getRunnerType, runnerType)
                .orderByDesc(OperLog::getCreateTime);
        operLogService.page(iPage, wrapper);
        return R.success(iPage);
    }
}
