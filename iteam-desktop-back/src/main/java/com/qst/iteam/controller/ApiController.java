package com.qst.iteam.controller;

import cn.hutool.json.JSONUtil;
import com.qst.iteam.domain.Api;
import com.qst.iteam.service.ApiService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@io.swagger.annotations.Api(tags = "Api管理接口")
public class ApiController {
    @Autowired
    private ApiService apiService;
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @GetMapping("/list")
    @ApiOperation("获取Api列表")
    public R<List<Api>> list() {
        List<Api> apiList = apiService.list();
        return R.success(apiList);
    }

    @GetMapping("/check")
    @ApiOperation("接口连通性检测")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", value = "api的id", required = true, paramType = "query", example = "2")
    })
    public R<Object> check(Long apiId) {
        if (apiId == null) {
            return R.error("请求字段缺失");
        }
        Api api = apiService.getById(apiId);
        if (api == null) {
            return R.error("id有误,未找到该Api");
        }
        // 获取path
        String path = api.getPath();
        // 构建绝对路径,RestTemplate要求是绝对路径
        String url = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(path)
                .toUriString();
        // 判断有没有参数,怎么传参数
        if (api.getParamType() == null) {
            // 无参
            restTemplateBuilder.build().getForObject(url, String.class);
        } else if (api.getParamType().contains("path") || api.getParamType().contains("query")) {
            // path参数
            url += api.getParamExample();
            if ("GET".equals(api.getMethod())) {
                restTemplateBuilder.build().getForObject(url, String.class);
            } else if ("DELETE".equals(api.getMethod())) {
                restTemplateBuilder.build().delete(url, String.class);
            }
        } else if (api.getParamType().contains("body")) {
            String body = api.getParamExample();
            restTemplateBuilder.build().postForObject(url, JSONUtil.toBean(body, Map.class), String.class);
        }
        return R.success();
    }

    @PostMapping("/insert")
    @ApiOperation("新增接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "api", value = "接口实体", required = true, paramType = "body",
                    example = "{\"path\": \"/admin/list\", \"method\": \"GET\", " +
                            "\"intro\": \"管理员列表\", \"visit\": 0}")
    })
    public R<Object> insert(@RequestBody Api api) {
        if (api == null) {
            return R.error("请求字段缺失");
        }
        boolean success = apiService.save(api);
        return success ? R.success() : R.error();
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据apiId删除接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", value = "api的id", required = true, paramType = "query", example = "87")
    })
    public R<Object> delete(Long apiId) {
        if (apiId == null) {
            return R.error("请求字段缺失");
        }
        boolean success = apiService.removeById(apiId);
        return success ? R.success() : R.error();
    }

    @PostMapping("/update")
    @ApiOperation("修改接口信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "api", value = "要修改的api实体,必须包含id字段", required = true, paramType = "body",
                    example = "{\"id\": 66, \"method\": \"POST\"}")
    })
    public R<Object> update(@RequestBody Api api) {
        if (api == null || api.getId() == null) {
            return R.error("请求字段缺失");
        }
        boolean success = apiService.updateById(api);
        return success ? R.success() : R.error();
    }
}
