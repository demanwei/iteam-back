package com.qst.iteam.controller;

import com.qst.iteam.domain.Rule;
import com.qst.iteam.service.RuleService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rule")
@Api(tags = "预警规则接口")
public class RuleController {
    @Autowired
    private RuleService ruleService;

    @GetMapping("/list")
    @ApiOperation("获取预警规则列表")
    public R<List<Rule>> list() {
        List<Rule> ruleList = ruleService.list();
        return R.success(ruleList);
    }

    @PostMapping("/insert")
    @ApiOperation("新增预警规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rule", value = "预警规则实体", required = true, paramType = "body",
                    example = "{\"metricName\": \"cpu_usage\", \"metricValue\": 80, \"metricUnit\": \"percent\", \"adminId\": 1}")
    })
    public R<Object> insert(@RequestBody Rule rule) {
        if (rule == null) {
            return R.error("请求字段缺失");
        }
        boolean success = ruleService.save(rule);
        return success ? R.success() : R.error();
    }

    @PostMapping("/update")
    @ApiOperation("修改预警规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rule", value = "预警规则实体", required = true, paramType = "body",
                    example = "{\"id\": 1, \"metricName\": \"cpu_usage\", \"metricValue\": 90, \"metricUnit\": \"percent\"}")
    })
    public R<Object> update(@RequestBody Rule rule) {
        if (rule == null || rule.getId() == null) {
            return R.error("请求字段缺失");
        }
        boolean success = ruleService.updateById(rule);
        return success ? R.success() : R.error();
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据id删除预警规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ruleId", value = "规则id", required = true, paramType = "query", example = "2")
    })
    public R<Object> delete(Long ruleId) {
        if (ruleId == null) {
            return R.error("请求字段缺失");
        }
        boolean success = ruleService.removeById(ruleId);
        return success ? R.success() : R.error();
    }
}
