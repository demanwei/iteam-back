package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Alert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AlertMapper extends BaseMapper<Alert> {
}
