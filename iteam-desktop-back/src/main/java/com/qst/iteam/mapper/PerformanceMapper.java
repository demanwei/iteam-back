package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Performance;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PerformanceMapper extends BaseMapper<Performance> {
}
