package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Api;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ApiMapper extends BaseMapper<Api> {
}
