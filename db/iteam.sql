/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : iteam

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 03/08/2023 13:58:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for it_chat
-- ----------------------------
DROP TABLE IF EXISTS `it_chat`;
CREATE TABLE `it_chat`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id_from` bigint(20) UNSIGNED NOT NULL,
  `user_id_to` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(10240) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_from` int(11) NULL DEFAULT 1 COMMENT '发送方状态:0正常,1已删除',
  `status_to` int(11) NULL DEFAULT 1 COMMENT '接收方状态:0正常,1已删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_chat`(`user_id_from`) USING BTREE,
  INDEX `user_chat_2`(`user_id_to`) USING BTREE,
  CONSTRAINT `user_chat` FOREIGN KEY (`user_id_from`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_chat_2` FOREIGN KEY (`user_id_to`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_chat
-- ----------------------------
INSERT INTO `it_chat` VALUES (1, 1, 7, '你好!', 0, 0, '2023-07-06 10:56:14');
INSERT INTO `it_chat` VALUES (2, 1, 7, '在吗?', 0, 0, '2023-07-10 02:49:01');
INSERT INTO `it_chat` VALUES (3, 7, 1, '在的', 0, 0, '2023-07-10 02:55:26');
INSERT INTO `it_chat` VALUES (4, 1, 7, '今天是周几?', 0, 0, '2023-07-10 03:40:53');
INSERT INTO `it_chat` VALUES (5, 7, 1, '周二吧', 0, 0, '2023-07-10 03:41:45');
INSERT INTO `it_chat` VALUES (6, 7, 1, '忘记了', 0, 0, '2023-07-10 03:43:28');
INSERT INTO `it_chat` VALUES (7, 1, 7, '好的', 0, 0, '2023-07-10 03:46:46');
INSERT INTO `it_chat` VALUES (8, 1, 7, '谢谢', 0, 0, '2023-07-10 06:59:29');

-- ----------------------------
-- Table structure for it_event
-- ----------------------------
DROP TABLE IF EXISTS `it_event`;
CREATE TABLE `it_event`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '活动名称',
  `intro` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '简介',
  `addr` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_img` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '活动头像地址',
  `start_time` datetime(0) NULL DEFAULT NULL,
  `lon` double NULL DEFAULT NULL COMMENT '经度',
  `lat` double NULL DEFAULT NULL COMMENT '纬度',
  `state` int(11) NULL DEFAULT 1 COMMENT '活动状态，默认为1，表示正常。2：已取消',
  `if_delete` int(11) NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `it_event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_event
-- ----------------------------
INSERT INTO `it_event` VALUES (1, 1, '周末夜跑', '户外装备要求：双肩背包,登山或徒步鞋，手杖(建议双杖），护膝，水壶，速干衣裤，保暖外套，帽子头巾手套，头灯或手电', '山东省青岛市市北区沈阳路17号', '/upload/event/logo/event_head_run.png', '2017-05-05 15:46:58', 120.374706370592, 36.0942773885701, 1, 0, '2023-07-27 10:36:25', '2023-07-27 10:36:27');
INSERT INTO `it_event` VALUES (2, 2, '环城骑行', '【集中时间】：1月21日晚上19∶30。【线路计划】：沙坪坝·站西路—芭蕉沟—香樟林—仙子井—仙女湖—同心岛—红休所—川外校园—烈士墓。（备注：具体线路根据当时天气等情况作调整。）【集合地点】：沙坪坝区·站西路黄桷树堡坎。', '山东省青岛市市北区洮南路', '/upload/event/logo/bg_event_header1.jpg', '2017-04-06 15:47:02', 120.370431393385, 36.0995515411286, 1, 0, '2023-07-27 10:36:25', '2023-07-27 10:36:27');
INSERT INTO `it_event` VALUES (3, 4, '月季山滑雪', '雪场离青岛较近，游玩时间比较长，可在兜里带些零食防止中途肚子饿。滑雪速度很快，不要碰撞到别人，也要避免别人冲撞到你。本次活动由QQ群：226748966 负责解释。', '山东省青岛市市北区大名路178号', '/upload/event/logo/huaxue.png', '2017-06-23 15:47:11', 120.372169464827, 36.0984321691284, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (4, 5, '真人CS对抗赛', '活动时长：4个小时集合时间：13:30集合地点：崂山区政府北门公交站天气预报：晴间多云 22度 南风5级收取费用：40元/人联系报名：群226748966', '山东省青岛市市北区威海路198号 ', '/upload/event/logo/cs.png', '2017-05-18 15:47:24', 120.372559726238, 36.0971935811387, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (5, 3, '登太行山', '活动路线：青岛－太行山－青岛相关景点：王莽岭景区活动强度：★★★☆☆[户外强度等级划分]活动时长：两天两夜集合时间：见活动内说明集合地点：见活动内说明天气预报：多云 13/28度 北风3-4级收取费用：560元/人联系报名：群58985561@花木通幽', '山东省青岛市市北区洮南路01号', '/upload/event/logo/taihangshan.png', '2017-04-20 15:47:07', 120.374391674995, 36.0962941568019, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (6, 6, '天琴座流星雨', '22日下午上岛，到宿营地点设营，晚上生篝火(如果允许)，看天琴座流星雨，拍美丽星空，第二天看海上日出，到周围游玩。午饭后出发返程，下午13时前离岛，17点前返回青岛。', '山东省青岛市崂山区青大路 ', '/upload/event/logo/liuxingyu.png', '2017-05-23 15:47:27', 120.375734120607, 36.0968013033564, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (7, 7, '油菜花田休闲游', '青岛也有油菜花田，坐落在胶南大珠山畔的西海岸生态观光园内种植了100亩的油菜花，现在正是盛花期，喜欢油菜花的一定要去这里，满足你金黄色的愿望。', '山东省青岛市哈尔滨路7号附近', '/upload/event/logo/youcaihua.png', '2017-05-09 15:47:30', 120.377040356398, 36.0982923819599, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (8, 3, '溪畔钓鱼垂钓同', '在清晨，挑选一根鱼竿，找一个附近的溪流，钓一些鲜活的鱼。邀上好友，分享收获，细品生活。', '山东省青岛市市北区水清沟街道开平路6丁9号水清沟东山公园', '/upload/event/default.png', '2023-07-31 16:08:34', 120.383240282536, 36.1359706471639, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (9, 3, '\r\n田园自行车穿越', '租一辆自行车，沿着田野小路骑行，感受田园和自然风光，追逐那份宁静和简单。', '山东省青岛市崂山区中韩街道浮山森林公园', '/upload/event/default.png', '2023-07-31 16:08:37', 120.427284836769, 36.0876526567836, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (10, 3, '宁静山涧徒步行', '踏上一段徒步旅程，沿着山涧漫步，在茂盛的树林和清流里散步，享受健康、平衡和安宁。', '山东省青岛市市南区香港中路街道五四广场', '/upload/event/default.png', '2023-07-25 14:06:00', 120.384517014027, 36.0617929248005, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (11, 3, '草原马术骑行行', '跨上骑马的姿态，悠然走在草原上，欣赏阳光和自由，沉浸在异域风情的体验中，领略大自然带来的美好。', '山东省青岛市市北区水清沟街道青岛北岭山森林公园', '/upload/event/default.png', '2023-07-31 16:08:40', 120.370006263256, 36.1207645800784, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (12, 3, '桃花源竹筏漂流', '在冰清玉洁的河水上漂流，欣赏周围的景色，体验与自然和谐相处的快乐。', '山东省青岛市李沧区虎山路街道黑龙江中路506号青岛禽鸣苑-十梅庵公园', '/upload/event/default.png', '2023-07-31 16:08:40', 120.4196177423, 36.1888753555856, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (13, 3, '沙漠沙滩越野行', '挑战沙漠和沙滩，通过各种越野运动，体验风沙的肆虐，和大自然力量的碰撞。', '山东省青岛市市北区威海路198号 ', '/upload/event/logo/20200701152829.jpg', '2023-07-31 16:08:40', 120.383240282536, 36.1359706471639, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (14, 3, '森林木屋露营行', '在森林中搭起木屋，徜徉在碧绿的森林、清新的空气中，享受远离城市喧嚣的难得宁静。', '山东省青岛市市北区洮南路01号', '/upload/event/logo/20200704003403.jpg', '2023-07-31 16:08:40', 120.427284836769, 36.0876526567836, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (15, 3, '海滨钓鱼航海行', '漫游在宽广的海洋上，钓到一些美味的鱼，听着海浪声潇洒航行，彻底放松身心。', '山东省青岛市崂山区青大路 ', '/upload/event/logo/20200704004315.jpg', '2023-07-31 16:08:40', 120.383240282536, 36.1359706471639, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (16, 3, '惬意海钓日', '在海边享受惬意的一天，体验垂钓和野餐，品尝一些新鲜的海产，舒缓心灵。', '山东省青岛市哈尔滨路7号附近', '/upload/event/logo/20200707233921.jpg', '2023-07-31 16:08:40', 120.427284836769, 36.0876526567836, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (17, 3, '恬静湖泊划船', '在湖泊上划船，欣赏绵延的山峦和潋滟的水光，感受宁静和和谐的氛围。', '山东省青岛市市北区水清沟街道开平路6丁9号水清沟东山公园', '/upload/event/logo/20200711211350.jpg', '2023-07-31 16:08:40', 120.073148, 35.933505, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (18, 3, '草原马术骑行', '沉醉于大草原的美丽风景，感受马儿奔跑带来的激情与自由，草原马术骑行活动，将带您深入茫茫草原，拥抱大自然，放飞心情，在马儿嘶鸣声中，感受与马儿并肩奔跑的快乐。', '山东省青岛市崂山区中韩街道浮山森林公园', '/upload/event/logo/20200711211509.jpg', '2023-07-31 16:08:40', 120.070655, 35.933593, 1, 0, '2023-07-07 10:36:40', '2023-07-14 10:36:49');
INSERT INTO `it_event` VALUES (19, 1, '周末夜跑', '磨砺意志、释放身心，让我们一起冲破黑夜的束缚，跑进更美好的生活！这次夜跑汇聚了全城跑者，我们将穿过城市的每一个角落，感受这座城市的迷人之处，成为夜色中一道亮丽的风景。', '山东省青岛市黄岛区大场镇吉利河路24号', '/upload/event/default.png', '2023-07-31 16:08:40', 120.374706370592, 36.0942773885701, 1, 0, '2023-07-27 11:27:40', '2023-07-27 11:27:40');

-- ----------------------------
-- Table structure for it_event_comment
-- ----------------------------
DROP TABLE IF EXISTS `it_event_comment`;
CREATE TABLE `it_event_comment`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `event_id`(`event_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `it_event_comment_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `it_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `it_event_comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_event_comment
-- ----------------------------
INSERT INTO `it_event_comment` VALUES (1, 1, 2, '夜幕下的街道看起来与白天大不相同，一场与黑暗搏斗的征程，考验着个人的毅力。', '2017-04-27 16:25:06');
INSERT INTO `it_event_comment` VALUES (2, 1, 6, '在沙漠中骑骆驼，欣赏广袤沙漠的壮美景色', '2017-04-12 20:25:47');
INSERT INTO `it_event_comment` VALUES (3, 1, 7, '挂上滑翔伞，翱翔于云端，感受飞翔的自由。', '2017-04-03 16:26:19');
INSERT INTO `it_event_comment` VALUES (4, 1, 1, '那种钓到鱼的感觉真的太棒了！', '2017-05-02 18:07:10');
INSERT INTO `it_event_comment` VALUES (5, 1, 2, '这是一条测试信息', '2017-05-02 18:09:28');
INSERT INTO `it_event_comment` VALUES (6, 1, 3, '拼尽全力压趴了沙丘，然后我们笑得像世界上最幸福的人', '2020-07-02 00:05:58');
INSERT INTO `it_event_comment` VALUES (7, 2, 4, '露营可以让你彻底放空，享受大自然！', '2020-07-04 00:54:43');
INSERT INTO `it_event_comment` VALUES (8, 1, 3, '海风拂面，阳光洒身，舒适惬意！', '2020-07-04 00:54:53');
INSERT INTO `it_event_comment` VALUES (9, 2, 2, '漂流于清澈的河流中，探寻自然之美。', '2020-07-04 22:39:36');
INSERT INTO `it_event_comment` VALUES (10, 4, 1, '独自划船，好像船是自己的，湖泊也是', '2023-07-19 11:49:30');
INSERT INTO `it_event_comment` VALUES (11, 4, 4, '月光洒在街道上，夜幕下的景色如诗如画，仿佛进入了一个神秘的梦境。', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (12, 2, 5, '夜晚的城市犹如一颗星星闪烁，探索夜幕下的街道给人带来无尽的惊喜与感动。', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (13, 8, 9, '沙漠中的黄沙延绵起伏，夕阳下呈现出金色的辉煌，仿佛走进了另一个世界。', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (14, 4, 8, '沿着沙漠的边缘缓缓行进，远处的沙丘如同波浪一般，让人心生敬畏。', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (15, 2, 3, '滑翔伞飞翔在碧蓝的天空中，仰望云朵，感受到一种前所未有的自由与轻盈', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (16, 2, 7, '滑翔伞在空中转动，令人忘记一切烦恼，只专注于那一刻的刺激与畅快。', '2023-08-01 10:26:53');
INSERT INTO `it_event_comment` VALUES (17, 3, 5, '钓鱼的时刻总是充满期待，当鱼儿上钓时，那种成就感是无法言喻的', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (18, 3, 1, '在湖边垂钓，享受宁静的时光，感受着大自然的和谐与安宁。', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (19, 6, 4, '踩着细软的沙子，爬上沙丘的顶峰，眺望远方的壮观景色，仿佛站在世界的巅峰。', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (20, 7, 2, '沙丘一片沉寂，只有风吹过的声音，与沙子激动碰撞的声响，是沙漠的独特乐曲。', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (21, 8, 1, '在野外搭起帐篷，与朋友围坐篝火，分享故事，欣赏星空，这是一种释放和感恩的体验。', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (22, 9, 1, '露营的时候真正感受到与大自然融为一体，洗去城市的喧嚣和压力，找回内心的平静与愉悦。', '2023-08-02 10:27:00');
INSERT INTO `it_event_comment` VALUES (23, 3, 9, '海风拂面，迎着清凉的海风漫步在沙滩上，思绪放飞，心灵也跟着舒展开来。', '2023-08-03 10:27:05');
INSERT INTO `it_event_comment` VALUES (24, 4, 5, '远眺湛蓝的海平线，海风轻轻扬起的味道让人陶醉，仿佛置身于一个美妙的世界', '2023-08-03 10:27:05');
INSERT INTO `it_event_comment` VALUES (25, 5, 1, '漂流于溪流中，随着水流的起伏，感受大自然的洗礼与涤荡，拥抱一种新鲜与活力。', '2023-08-03 10:27:05');
INSERT INTO `it_event_comment` VALUES (26, 6, 2, '沿着曲折而澄澈的河流漂流，静享大自然带来的无限宁静与疗愈。', '2023-08-03 10:27:05');
INSERT INTO `it_event_comment` VALUES (27, 9, 5, '独自划船在湖面上，只有船桨划水的声音和周围的静谧，让人心旷神怡，感受真正的宁静与自由。', '2023-08-03 10:27:05');
INSERT INTO `it_event_comment` VALUES (28, 7, 7, '划船穿行在宁静的湖泊之间，水波微荡，让人体验到一种与大自然融合的奇妙感觉。', '2023-08-03 10:27:05');

-- ----------------------------
-- Table structure for it_event_member
-- ----------------------------
DROP TABLE IF EXISTS `it_event_member`;
CREATE TABLE `it_event_member`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `event_id`(`event_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `it_event_member_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `it_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `it_event_member_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_event_member
-- ----------------------------
INSERT INTO `it_event_member` VALUES (2, 1, 3, '2017-05-03 16:53:49');
INSERT INTO `it_event_member` VALUES (3, 2, 7, '2023-07-10 02:55:05');
INSERT INTO `it_event_member` VALUES (4, 3, 7, '2017-05-01 17:34:40');
INSERT INTO `it_event_member` VALUES (5, 4, 7, '2017-05-05 15:29:24');
INSERT INTO `it_event_member` VALUES (6, 5, 7, '2017-04-18 17:34:49');
INSERT INTO `it_event_member` VALUES (7, 6, 7, '2017-05-05 15:29:44');
INSERT INTO `it_event_member` VALUES (8, 7, 1, '2017-05-13 17:35:05');
INSERT INTO `it_event_member` VALUES (9, 12, 4, '2020-07-04 00:35:02');
INSERT INTO `it_event_member` VALUES (10, 3, 4, '2020-07-04 00:43:55');
INSERT INTO `it_event_member` VALUES (11, 12, 4, '2020-07-04 00:45:08');
INSERT INTO `it_event_member` VALUES (14, 2, 2, '2020-07-08 02:21:04');
INSERT INTO `it_event_member` VALUES (15, 2, 6, '2020-07-08 02:54:01');
INSERT INTO `it_event_member` VALUES (16, 1, 5, '2020-07-11 21:05:32');
INSERT INTO `it_event_member` VALUES (17, 3, 1, '2023-07-10 07:00:56');
INSERT INTO `it_event_member` VALUES (18, 1, 2, '2023-07-19 16:16:34');

-- ----------------------------
-- Table structure for it_friend
-- ----------------------------
DROP TABLE IF EXISTS `it_friend`;
CREATE TABLE `it_friend`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `friend_id` bigint(20) UNSIGNED NOT NULL,
  `apply` int(11) NULL DEFAULT NULL COMMENT '申请状态：0表示拒绝，1表示同意，2表示申请中',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`, `friend_id`) USING BTREE,
  INDEX `friend_id`(`friend_id`) USING BTREE,
  CONSTRAINT `it_friend_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `it_friend_ibfk_2` FOREIGN KEY (`friend_id`) REFERENCES `it_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_friend
-- ----------------------------
INSERT INTO `it_friend` VALUES (1, 1, 4, 1, '2023-07-18 15:19:38');
INSERT INTO `it_friend` VALUES (2, 2, 5, 2, '2023-07-18 15:19:48');
INSERT INTO `it_friend` VALUES (3, 2, 7, 2, '2023-07-18 15:19:38');
INSERT INTO `it_friend` VALUES (4, 2, 6, 1, '2023-07-18 15:19:48');
INSERT INTO `it_friend` VALUES (5, 3, 2, 1, '2023-07-18 15:19:38');
INSERT INTO `it_friend` VALUES (6, 3, 1, 1, '2023-07-18 15:19:48');
INSERT INTO `it_friend` VALUES (7, 6, 1, 1, '2023-07-18 15:20:00');
INSERT INTO `it_friend` VALUES (8, 7, 2, 1, '2023-07-18 15:19:38');
INSERT INTO `it_friend` VALUES (9, 4, 3, 2, '2023-07-18 15:19:48');
INSERT INTO `it_friend` VALUES (10, 5, 7, 2, '2023-07-18 15:19:45');
INSERT INTO `it_friend` VALUES (11, 1, 2, 2, '2023-07-18 15:19:38');
INSERT INTO `it_friend` VALUES (12, 3, 5, 1, '2023-07-19 11:14:59');

-- ----------------------------
-- Table structure for it_user
-- ----------------------------
DROP TABLE IF EXISTS `it_user`;
CREATE TABLE `it_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `addr` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '地址',
  `gender` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '性别',
  `head_img` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '头像',
  `lon` double NULL DEFAULT NULL,
  `lat` double NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of it_user
-- ----------------------------
INSERT INTO `it_user` VALUES (1, 'aaa', 'aaa', '失忆d', '山东省青岛市黄岛区铁山街道青岛杨家山里四季果生态园专业合作社', '1', '/upload/user/38.png', 119.866775, 35.904506, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (2, 'bbb', 'bbb', '寂静 sH〃', '山东省青岛市市北区沈阳路17号', '1', '/upload/user/39.png', 119.866775, 35.904506, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (3, 'ccc', 'ccc', '掩饰的心伤', '山东省青岛市市南区香港中路街道五四广场', '0', '/upload/user/40.png', 119.866775, 35.904506, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (4, 'ddd', 'ddd', '青春起点', '山东省青岛市李沧区虎山路街道黑龙江中路506号青岛禽鸣苑-十梅庵公园', '0', '/upload/user/41.png', 119.866775, 35.904506, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (5, 'eee', 'eee', '迷梦D', '山东省青岛市市北区水清沟街道青岛北岭山森林公园', '0', '/upload/user/44.png', 119.866775, 35.904506, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (6, 'fff', 'fff', 'JackWill', '山东省青岛市黄岛区隐珠街道珠光集团幼儿园山水文苑', '1', '/upload/user/20200711210506.jpg', 120.073242, 35.93361, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (7, 'ggg', 'ggg', '莫忘初心', '山东省青岛市黄岛区大场镇吉利河路', '0', '/upload/user/20200709131314.jpg', 120.373703688383, 36.0992698033079, '2023-07-26 17:39:38');
INSERT INTO `it_user` VALUES (8, 'hhh', 'hhh', '魏德曼', '北京市海淀区上园村3号', '0', '/upload/user/lzc.png', 119.866775, 35.904506, '2023-07-27 11:30:38');
INSERT INTO `it_user` VALUES (9, 'iii', 'iii', '草莓冰淇淋女士', '山东省青岛市即墨区滨海路72号', '1', '/upload/user/lmj.png', 119.866775, 35.904506, '2023-07-27 11:33:42');

-- ----------------------------
-- Table structure for op_admin
-- ----------------------------
DROP TABLE IF EXISTS `op_admin`;
CREATE TABLE `op_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `end` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receive_email` int(11) NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_admin
-- ----------------------------
INSERT INTO `op_admin` VALUES (1, '90217', 'a906449d5769fa7361d7ecc6aa3f6d28', 'desktop', 'xxx@163.com', 0, '2023-07-14 09:14:18');
INSERT INTO `op_admin` VALUES (2, 'admin', 'a906449d5769fa7361d7ecc6aa3f6d28', 'desktop', 'xxx@qq.com', 1, '2023-07-14 10:32:45');
INSERT INTO `op_admin` VALUES (3, '90217', 'a906449d5769fa7361d7ecc6aa3f6d28', 'web', NULL, 0, '2023-07-14 09:14:18');
INSERT INTO `op_admin` VALUES (4, 'Azir', 'a906449d5769fa7361d7ecc6aa3f6d28', 'web', NULL, 1, '2023-07-14 10:32:45');
INSERT INTO `op_admin` VALUES (5, 'test111', 'a906449d5769fa7361d7ecc6aa3f6d28', 'desktop', NULL, 0, '2023-08-03 10:42:49');

-- ----------------------------
-- Table structure for op_alert
-- ----------------------------
DROP TABLE IF EXISTS `op_alert`;
CREATE TABLE `op_alert`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `metric_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `metric_value_real` float NULL DEFAULT NULL,
  `metric_value_rule` float NULL DEFAULT NULL,
  `metric_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_alert
-- ----------------------------
INSERT INTO `op_alert` VALUES (1, 'cpu_usage', 87.94, 80, 'percent', '2023-07-25 10:16:33');
INSERT INTO `op_alert` VALUES (2, 'cpu_usage', 92.13, 80, 'percent', '2023-07-31 16:03:52');
INSERT INTO `op_alert` VALUES (3, 'disk_usage', 82, 80, 'percent', '2023-07-31 16:04:25');
INSERT INTO `op_alert` VALUES (4, 'memory_usage', 96.12, 90, 'percent', '2023-07-31 16:05:14');
INSERT INTO `op_alert` VALUES (5, 'disk_usage', 85, 80, 'percent', '2023-07-31 16:05:33');
INSERT INTO `op_alert` VALUES (6, 'cpu_usage', 82.33, 80, 'percent', '2023-07-31 13:17:53');
INSERT INTO `op_alert` VALUES (7, 'disk_usage', 85.34, 80, 'percent', '2023-07-31 16:04:25');
INSERT INTO `op_alert` VALUES (8, 'memory_usage', 90.14, 90, 'percent', '2023-07-31 16:05:14');
INSERT INTO `op_alert` VALUES (9, 'memory_used', 11.28, 9, 'GB', '2023-08-01 11:35:53');
INSERT INTO `op_alert` VALUES (10, 'memory_used', 11.27, 9, 'GB', '2023-08-01 11:36:02');
INSERT INTO `op_alert` VALUES (11, 'memory_used', 9.21, 9, 'GB', '2023-08-03 10:44:02');
INSERT INTO `op_alert` VALUES (12, 'memory_used', 9.2, 9, 'GB', '2023-08-03 10:44:12');
INSERT INTO `op_alert` VALUES (13, 'memory_used', 9.29, 9, 'GB', '2023-08-03 10:44:22');
INSERT INTO `op_alert` VALUES (14, 'memory_used', 10.31, 9, 'GB', '2023-08-03 13:55:13');

-- ----------------------------
-- Table structure for op_api
-- ----------------------------
DROP TABLE IF EXISTS `op_api`;
CREATE TABLE `op_api`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `end` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `intro` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param_example` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `response_body` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `visit` bigint(20) UNSIGNED NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `end_path`(`end`, `path`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_api
-- ----------------------------
INSERT INTO `op_api` VALUES (1, 'desktop', '/admin/register', '新增管理员', 'POST', 'body', '{\"username\":\"test111\", \"password\": \"123abc\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (2, 'desktop', '/admin/list', '获取管理员列表', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Admin>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (3, 'desktop', '/admin/login', '管理员登录', 'POST', 'body', '{\"username\":\"90217\", \"password\": \"123abc\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (4, 'desktop', '/admin/info', '根据id获取管理员信息', 'GET', 'query', '?adminId=1', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (5, 'desktop', '/alert/recent', '预警信息分页查询', 'GET', 'query,query', '?current=1&size=20', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.Alert>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (6, 'desktop', '/api/update', '修改接口信息', 'POST', 'body', '{\"id\": 66, \"method\": \"POST\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (7, 'desktop', '/api/delete', '根据apiId删除接口', 'DELETE', 'query', '?apiId=87', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (8, 'desktop', '/api/check', '接口连通性检测', 'GET', 'query', '?apiId=2', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (9, 'desktop', '/api/list', '获取Api列表', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Api>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (10, 'desktop', '/api/insert', '新增接口', 'POST', 'body', '{\"path\": \"/admin/list\", \"method\": \"GET\", \"intro\": \"管理员列表\", \"visit\": 0}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (11, 'desktop', '/db/check', '数据库连接状态检测', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.lang.Integer', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (12, 'desktop', '/db/info', '查询数据库元数据信息', 'GET', NULL, NULL, 'success=boolean, info=string, obj=com.qst.iteam.domain.DbInfo', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (13, 'desktop', '/operLog/byAdmin', '根据adminId分页查询操作日志列表', 'GET', 'query,query,query', '?adminId=1&current=1&size=20', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.OperLog>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (14, 'desktop', '/operLog/byRunnerType', '根据操作者类型分页查询操作日志列表', 'GET', 'query,query,query', '?runnerType=1&current=1&size=20', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.OperLog>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (15, 'desktop', '/performance/recent', '监控记录分页查询', 'GET', 'query,query', '?current=1&size=20', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.Performance>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (16, 'desktop', '/rule/update', '修改预警规则', 'POST', 'body', '{\"id\": 1, \"metricName\": \"cpu_usage\", \"metricValue\": 90, \"metricUnit\": \"percent\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (17, 'desktop', '/rule/delete', '根据id删除预警规则', 'DELETE', 'query', '?ruleId=2', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (18, 'desktop', '/rule/list', '获取预警规则列表', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Rule>', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (19, 'desktop', '/rule/insert', '新增预警规则', 'POST', 'body', '{\"metricName\": \"cpu_usage\", \"metricValue\": 80, \"metricUnit\": \"percent\", \"adminId\": 1}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:54:57');
INSERT INTO `op_api` VALUES (20, 'mobile', '/chat/delete', '根据chatId删除聊天记录', 'DELETE', 'query,query,query', '?chatId=1&userIdFrom=1&userIdTo=2', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (21, 'mobile', '/chat/insert', '新增聊天记录', 'POST', 'body', '{\"userIdFrom\": 1, \"userIdTo\": 2, \"content\":\"Hello!\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (22, 'mobile', '/chat/byUserIdFromAndTo', '根据两个用户的id获取聊天记录列表', 'GET', 'query,query', '?userIdFrom=1&userIdTo=2', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Chat>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (23, 'mobile', '/eventComment/insert', '新增评论', 'POST', 'body', '{\"eventId\": 1, \"userId\": 1, \"content\": \"Hahahahaha\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (24, 'mobile', '/eventComment/listByEventId', '根据eventId查找所有相关评论', 'GET', 'query', '?eventId=1', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.dto.EventCommentDto>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (25, 'mobile', '/event/delete', '删除活动信息', 'DELETE', 'query', '?eventId=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (26, 'mobile', '/event/list', '获取活动列表|搜索活动', 'GET', 'query,query', '?kw=骑行&userId=1', 'success=boolean, info=string, obj=java.util.List<? extends com.qst.iteam.domain.Event>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (27, 'mobile', '/event/insert', '新增活动信息', 'POST', 'body', '{\"name\": \"插入活动测试\", \"userId\": 1, \"intro\": \"活动内容....\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (28, 'mobile', '/event/isJoin', '判断某用户是否参加了某个活动', 'GET', 'query,query', '?eventId=1&userId=1', 'success=boolean, info=string, obj=java.lang.Integer', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (29, 'mobile', '/event/nearbyList', '获取附近活动列表', 'GET', 'query,query,query', '?targetLat=36.1&targetLont=120.4&distance=1.2', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Event>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (30, 'mobile', '/event/recommend', '活动推荐', 'GET', NULL, NULL, 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (31, 'mobile', '/event/info', '根据eventId获取某个活动的信息', 'GET', 'query', '?eventId=1', 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (32, 'mobile', '/eventMember/join', '加入活动', 'POST', 'form,form', '?eventId=1&userId=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (33, 'mobile', '/eventMember/teamList', '获取小队信息(我加入的所有活动列表)', 'GET', 'query', '?userId=1', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.dto.TeamDto>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (34, 'mobile', '/eventMember/list', '某个活动所有的成员列表', 'GET', 'query,query', '?eventId=1&userId=1', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.dto.EventMemberDto>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (35, 'mobile', '/friend/status', '查询当前用户和活动成员的关系：好友，已发送好友申请，未发送好友申请，被拒绝', 'GET', 'query,query', '?userId=1&memberId=2', 'success=boolean, info=string, obj=java.lang.Integer', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (36, 'mobile', '/friend/sendApply', '发送好友申请', 'POST', 'form,form', '?userId=1&friendId=2', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (37, 'mobile', '/friend/processApply', '处理好友申请', 'POST', 'form,form,form', '?userId=2&friendId=1&apply=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (38, 'mobile', '/friend/getFriendList', '根据userId查询好友列表', 'GET', 'query', '?userId=1', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.User>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (39, 'mobile', '/friend/applyList', '根据userId查询申请列表', 'GET', 'query', '?userId=1', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.User>', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (40, 'mobile', '/upload/uploadFile', '上传文件', 'POST', 'form', '?file=', 'success=boolean, info=string, obj=java.lang.String', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (41, 'mobile', '/user/reg', '用户注册', 'POST', 'body', '{\"username\":\"aaaaaa\", \"password\": \"aaaaaa\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.User', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (42, 'mobile', '/user/update', '更新用户信息', 'PUT', 'body', '{\"id\": 2, \"username\":\"aaabbb\", \"password\": \"aaabbb\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (43, 'mobile', '/user/logout', '用户退出登录', 'POST', 'form', '?userId=3', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (44, 'mobile', '/user/login', '用户登录', 'POST', 'body', '{\"username\":\"aaa\", \"password\": \"aaa\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.User', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (45, 'mobile', '/user/uploadHeadImg', '上传用户头像', 'POST', 'form,form', '?userId=1&file=org.springframework.web.multipart.MultipartFile', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (46, 'mobile', '/user/info', '根据userId查询用户信息', 'GET', 'query', '?userId=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:05');
INSERT INTO `op_api` VALUES (47, 'web', '/admin/list', '获取管理员列表', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.Admin>', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (48, 'web', '/admin/insert', '新增管理员', 'POST', 'body', '{\"username\":\"test111\", \"password\": \"123abc\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (49, 'web', '/admin/info', '根据id获取管理员信息', 'GET', 'query', '?adminId=1', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (50, 'web', '/admin/login', '管理员登录', 'POST', 'body', '{\"username\":\"90217\", \"password\": \"123abc\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (51, 'web', '/admin/logout', '管理员退出登录', 'POST', NULL, NULL, 'success=boolean, info=string, obj=com.qst.iteam.domain.Admin', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (52, 'web', '/eventComment/insert', '新增评论', 'POST', 'body', NULL, 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (53, 'web', '/eventComment/delete', '根据id删除某条评论', 'DELETE', 'query', '?id=', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (54, 'web', '/eventComment/deleteByEventId', '根据eventId删除所有相关评论', 'DELETE', 'query', '?eventId=', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (55, 'web', '/eventComment/listByEventId', '根据eventId查找所有相关评论', 'GET', 'query', '?eventId=', 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.dto.EventCommentDto>', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (56, 'web', '/eventComment/listPage', '活动评论列表分页查询', 'GET', 'query,query,query', '?current=1&size=20&kw=骑行', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.EventComment>', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (57, 'web', '/event/update', '修改活动信息', 'PUT', 'body', NULL, 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (58, 'web', '/event/delete', '根据eventId删除活动', 'DELETE', 'query', '?eventId=', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (59, 'web', '/event/insert', '新增活动', 'POST', 'body', NULL, 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (60, 'web', '/event/info', '根据eventId获取某个活动的信息', 'GET', 'query', '?eventId=', 'success=boolean, info=string, obj=com.qst.iteam.domain.Event', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (61, 'web', '/event/listPage', '活动列表分页查询', 'GET', 'query,query,query', '?current=1&size=20&kw=草原', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.Event>', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (62, 'web', '/user/update', '更新用户信息', 'PUT', 'body', '{\"id\": 2, \"username\":\"aaabbb\", \"password\": \"aaabbb\"}', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (63, 'web', '/user/delete', '根据userId删除用户', 'DELETE', 'query', '?userId=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (64, 'web', '/user/list', '获取用户列表', 'GET', NULL, NULL, 'success=boolean, info=string, obj=java.util.List<com.qst.iteam.domain.User>', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (65, 'web', '/user/insert', '添加用户', 'POST', 'body', '{\"username\":\"aaaaaa\", \"password\": \"aaaaaa\"}', 'success=boolean, info=string, obj=com.qst.iteam.domain.User', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (66, 'web', '/user/info', '根据userId查询用户信息', 'GET', 'query', '?userId=1', 'success=boolean, info=string, obj=java.lang.Object', 0, '2023-08-03 13:55:08');
INSERT INTO `op_api` VALUES (67, 'web', '/user/listPage', '用户列表分页查询', 'GET', 'query,query,query', '?current=1&size=20&kw=青春', 'success=boolean, info=string, obj=com.baomidou.mybatisplus.core.metadata.IPage<com.qst.iteam.domain.User>', 0, '2023-08-03 13:55:08');

-- ----------------------------
-- Table structure for op_operlog
-- ----------------------------
DROP TABLE IF EXISTS `op_operlog`;
CREATE TABLE `op_operlog`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `runner_id` bigint(20) UNSIGNED NOT NULL,
  `runner_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `operlog_user`(`runner_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 165 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_operlog
-- ----------------------------
INSERT INTO `op_operlog` VALUES (63, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 10:37:29');
INSERT INTO `op_operlog` VALUES (64, 3, 'web', '/event/insert', 'name=test,intro=test,addr=test', '2023-07-27 10:38:05');
INSERT INTO `op_operlog` VALUES (65, 3, 'mobile', '/user/insert', 'username=test,password=test', '2023-07-27 10:39:24');
INSERT INTO `op_operlog` VALUES (66, 4, 'web', '/admin/login', 'username=Azir,password=123abc', '2023-07-27 10:41:33');
INSERT INTO `op_operlog` VALUES (67, 4, 'mobile', '/user/update', 'id=8,username=test,name=魏德曼,addr=test,gender=0', '2023-07-27 10:41:45');
INSERT INTO `op_operlog` VALUES (68, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 10:43:12');
INSERT INTO `op_operlog` VALUES (69, 3, 'mobile', '/user/delete', 'userId=8', '2023-07-27 10:43:18');
INSERT INTO `op_operlog` VALUES (70, 3, 'mobile', '/user/insert', 'username=test,password=123abc,name=test2', '2023-07-27 10:43:30');
INSERT INTO `op_operlog` VALUES (71, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 10:46:11');
INSERT INTO `op_operlog` VALUES (72, 3, 'web', '/eventComment/delete', 'id=11', '2023-07-27 10:46:21');
INSERT INTO `op_operlog` VALUES (73, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 10:48:28');
INSERT INTO `op_operlog` VALUES (74, 3, 'web', '/event/insert', 'name=魏德曼', '2023-07-27 10:48:49');
INSERT INTO `op_operlog` VALUES (75, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:02:30');
INSERT INTO `op_operlog` VALUES (76, 3, 'web', '/event/insert', 'userId=2,name=2', '2023-07-27 11:02:39');
INSERT INTO `op_operlog` VALUES (77, 3, 'web', '/event/insert', 'name=1,intro=2', '2023-07-27 11:02:45');
INSERT INTO `op_operlog` VALUES (78, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:03:27');
INSERT INTO `op_operlog` VALUES (79, 3, 'web', '/event/insert', 'name=32', '2023-07-27 11:03:33');
INSERT INTO `op_operlog` VALUES (80, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:05:17');
INSERT INTO `op_operlog` VALUES (81, 3, 'web', '/event/insert', 'name=21', '2023-07-27 11:05:22');
INSERT INTO `op_operlog` VALUES (82, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:05:50');
INSERT INTO `op_operlog` VALUES (83, 3, 'web', '/event/insert', 'name=123321321', '2023-07-27 11:05:59');
INSERT INTO `op_operlog` VALUES (84, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:18:01');
INSERT INTO `op_operlog` VALUES (85, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:18:50');
INSERT INTO `op_operlog` VALUES (86, 3, 'web', '/event/insert', 'userId=2,name=ee', '2023-07-27 11:19:05');
INSERT INTO `op_operlog` VALUES (87, 3, 'web', '/event/delete', 'eventId=20', '2023-07-27 11:19:12');
INSERT INTO `op_operlog` VALUES (88, 3, 'web', '/event/update', 'id=19,name=2,intro=2e,addr=234423', '2023-07-27 11:19:20');
INSERT INTO `op_operlog` VALUES (89, 3, 'web', '/event/delete', 'eventId=19', '2023-07-27 11:19:28');
INSERT INTO `op_operlog` VALUES (90, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:25:43');
INSERT INTO `op_operlog` VALUES (91, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:26:53');
INSERT INTO `op_operlog` VALUES (92, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:27:30');
INSERT INTO `op_operlog` VALUES (93, 3, 'web', '/event/insert', 'userId=1,name=waeea,intro=ea,addr=ewae', '2023-07-27 11:27:40');
INSERT INTO `op_operlog` VALUES (94, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:30:25');
INSERT INTO `op_operlog` VALUES (95, 3, 'mobile', '/user/insert', 'username=5555,password=123abc,name=55555', '2023-07-27 11:30:38');
INSERT INTO `op_operlog` VALUES (96, 3, 'mobile', '/user/update', 'id=8,username=5555,name=55555,gender=1', '2023-07-27 11:30:56');
INSERT INTO `op_operlog` VALUES (97, 3, 'mobile', '/user/update', 'id=8,username=5555,name=55555,gender=0', '2023-07-27 11:31:08');
INSERT INTO `op_operlog` VALUES (98, 3, 'web', '/admin/logout', NULL, '2023-07-27 11:32:53');
INSERT INTO `op_operlog` VALUES (99, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:33:05');
INSERT INTO `op_operlog` VALUES (100, 3, 'mobile', '/user/insert', 'username=test,password=123abc,name=test', '2023-07-27 11:33:42');
INSERT INTO `op_operlog` VALUES (101, 3, 'web', '/admin/logout', NULL, '2023-07-27 11:36:12');
INSERT INTO `op_operlog` VALUES (102, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:53:18');
INSERT INTO `op_operlog` VALUES (103, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 11:56:32');
INSERT INTO `op_operlog` VALUES (104, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 12:00:40');
INSERT INTO `op_operlog` VALUES (105, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:13:58');
INSERT INTO `op_operlog` VALUES (106, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:15:29');
INSERT INTO `op_operlog` VALUES (107, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:16:29');
INSERT INTO `op_operlog` VALUES (108, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:18:38');
INSERT INTO `op_operlog` VALUES (109, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:21:17');
INSERT INTO `op_operlog` VALUES (110, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:22:39');
INSERT INTO `op_operlog` VALUES (111, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:23:32');
INSERT INTO `op_operlog` VALUES (112, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:26:39');
INSERT INTO `op_operlog` VALUES (113, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:31:56');
INSERT INTO `op_operlog` VALUES (114, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:34:28');
INSERT INTO `op_operlog` VALUES (115, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:35:26');
INSERT INTO `op_operlog` VALUES (116, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:38:45');
INSERT INTO `op_operlog` VALUES (117, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:40:20');
INSERT INTO `op_operlog` VALUES (118, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-27 13:42:02');
INSERT INTO `op_operlog` VALUES (119, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:43:10');
INSERT INTO `op_operlog` VALUES (120, 3, 'mobile', '/user/update', 'id=9,username=test,name=test,gender=0', '2023-07-27 13:43:25');
INSERT INTO `op_operlog` VALUES (121, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:46:55');
INSERT INTO `op_operlog` VALUES (122, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:47:55');
INSERT INTO `op_operlog` VALUES (123, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 13:49:13');
INSERT INTO `op_operlog` VALUES (124, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-27 14:29:08');
INSERT INTO `op_operlog` VALUES (125, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-07-28 09:07:56');
INSERT INTO `op_operlog` VALUES (126, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 09:43:24');
INSERT INTO `op_operlog` VALUES (127, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 11:32:46');
INSERT INTO `op_operlog` VALUES (128, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 13:08:07');
INSERT INTO `op_operlog` VALUES (129, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 13:14:06');
INSERT INTO `op_operlog` VALUES (130, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 13:15:07');
INSERT INTO `op_operlog` VALUES (131, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 14:06:31');
INSERT INTO `op_operlog` VALUES (132, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-28 15:46:25');
INSERT INTO `op_operlog` VALUES (133, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-31 10:15:57');
INSERT INTO `op_operlog` VALUES (134, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-31 13:58:23');
INSERT INTO `op_operlog` VALUES (135, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-07-31 14:41:15');
INSERT INTO `op_operlog` VALUES (136, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 09:47:41');
INSERT INTO `op_operlog` VALUES (137, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 09:50:44');
INSERT INTO `op_operlog` VALUES (138, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 10:07:47');
INSERT INTO `op_operlog` VALUES (139, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 10:45:31');
INSERT INTO `op_operlog` VALUES (140, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 11:39:27');
INSERT INTO `op_operlog` VALUES (141, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 11:42:14');
INSERT INTO `op_operlog` VALUES (142, 3, 'web', '/admin/logout', NULL, '2023-08-01 11:43:36');
INSERT INTO `op_operlog` VALUES (143, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 12:58:11');
INSERT INTO `op_operlog` VALUES (144, 3, 'web', '/event/insert', 'userId=1,name=活动名称test,intro=活动介绍test,addr=山东省青岛市黄岛区,startTime=2023-08-01T13:22', '2023-08-01 13:26:16');
INSERT INTO `op_operlog` VALUES (145, 3, 'web', '/event/update', 'id=20,name=test,intro=test222,addr=山东省青岛市黄岛区,startTime=2023-08-01T13:22', '2023-08-01 13:29:20');
INSERT INTO `op_operlog` VALUES (146, 3, 'web', '/event/delete', 'eventId=20', '2023-08-01 13:38:50');
INSERT INTO `op_operlog` VALUES (147, 3, 'web', '/eventComment/delete', 'id=11', '2023-08-01 13:59:32');
INSERT INTO `op_operlog` VALUES (148, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 15:01:37');
INSERT INTO `op_operlog` VALUES (149, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-01 15:03:16');
INSERT INTO `op_operlog` VALUES (150, 3, 'web', '/user/insert', 'username=user_test,password=111111,name=user_test', '2023-08-01 15:09:39');
INSERT INTO `op_operlog` VALUES (151, 3, 'web', '/user/delete', 'userId=10', '2023-08-01 15:13:13');
INSERT INTO `op_operlog` VALUES (152, 3, 'web', '/user/insert', 'username=user_test,password=123abc1,name=user_test', '2023-08-01 15:18:05');
INSERT INTO `op_operlog` VALUES (153, 3, 'web', '/user/update', 'id=11,username=user_test,name=测试用户,gender=0', '2023-08-01 15:21:50');
INSERT INTO `op_operlog` VALUES (154, 3, 'web', '/user/delete', 'userId=11', '2023-08-01 15:23:07');
INSERT INTO `op_operlog` VALUES (155, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-08-02 11:21:10');
INSERT INTO `op_operlog` VALUES (156, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-08-03 09:11:27');
INSERT INTO `op_operlog` VALUES (157, 3, 'mobile', '/user/login', 'username=90217,password=123abc', '2023-08-03 09:22:46');
INSERT INTO `op_operlog` VALUES (158, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 09:37:56');
INSERT INTO `op_operlog` VALUES (159, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 09:39:10');
INSERT INTO `op_operlog` VALUES (160, 0, 'desktop', '/admin/register', 'username=test111,password=123abc', '2023-08-03 10:42:49');
INSERT INTO `op_operlog` VALUES (161, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 10:45:41');
INSERT INTO `op_operlog` VALUES (162, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 13:26:27');
INSERT INTO `op_operlog` VALUES (163, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 13:27:50');
INSERT INTO `op_operlog` VALUES (164, 3, 'web', '/admin/login', 'username=90217,password=123abc', '2023-08-03 13:28:55');

-- ----------------------------
-- Table structure for op_performance
-- ----------------------------
DROP TABLE IF EXISTS `op_performance`;
CREATE TABLE `op_performance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cpu_usage` float NULL DEFAULT NULL,
  `cpu_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `memory_total` float NULL DEFAULT NULL,
  `memory_used` float NULL DEFAULT NULL,
  `memory_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `net_rx` float NULL DEFAULT NULL,
  `net_tx` float NULL DEFAULT NULL,
  `net_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `disk_total` float NULL DEFAULT NULL,
  `disk_used` float NULL DEFAULT NULL,
  `disk_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `heap_used` float NULL DEFAULT NULL,
  `heap_max` float NULL DEFAULT NULL,
  `heap_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `non_heap_used` float NULL DEFAULT NULL,
  `non_heap_max` float NULL DEFAULT NULL,
  `non_heap_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_performance
-- ----------------------------

-- ----------------------------
-- Table structure for op_rule
-- ----------------------------
DROP TABLE IF EXISTS `op_rule`;
CREATE TABLE `op_rule`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `metric_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `metric_value` float NULL DEFAULT NULL,
  `metric_unit` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `metric_name`(`metric_name`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  CONSTRAINT `op_rule_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `op_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_rule
-- ----------------------------
INSERT INTO `op_rule` VALUES (1, 1, 'cpu_usage', 80, 'percent', '2023-07-14 17:02:18', '2023-07-14 17:02:21');
INSERT INTO `op_rule` VALUES (2, 2, 'memory_used', 14, 'GB', '2023-07-31 15:57:06', '2023-07-31 15:57:09');
INSERT INTO `op_rule` VALUES (3, 1, 'disk_used', 500, 'GB', '2023-07-31 15:57:27', '2023-07-31 15:57:30');
INSERT INTO `op_rule` VALUES (4, 1, 'net_rx', 9999, 'MBps', '2023-07-31 15:58:16', '2023-07-31 15:58:18');

SET FOREIGN_KEY_CHECKS = 1;
