package com.qst.iteam.utils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

public class ReflectUtils {
    public static boolean isPrimitive(Object o) {
        return o instanceof Integer || o instanceof Double ||
                o instanceof Boolean || o instanceof Character ||
                o instanceof Byte || o instanceof Short ||
                o instanceof Long || o instanceof Float ||
                o instanceof String;
    }

    public static boolean isHttp(Object o) {
        return o instanceof HttpSession ||
                o instanceof ServletRequest ||
                o instanceof ServletResponse;
    }
}
