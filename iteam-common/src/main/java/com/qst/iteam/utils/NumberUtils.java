package com.qst.iteam.utils;

public class NumberUtils {
    public static double round(double number, int round) {
        StringBuilder sb = new StringBuilder("1");
        for (int i = 0; i < round; i++) {
            sb.append("0");
        }
        int m = Integer.parseInt(sb.toString());
        return (double) Math.round(number * m) / m;
    }
}
