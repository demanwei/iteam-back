package com.qst.iteam.utils;

public class StringUtils {
    /**
     * 如果是路径参数请求,只获取前边的path,参数占位去掉
     */
    public static String getCorrectPath(String path) {
        if (path.contains("{") || path.contains("}")) {
            return path.split("/\\{")[0];
        }
        return path;
    }
}
