package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("it_chat")
@ApiModel("聊天记录")
public class Chat {
    private Long id;
    @ApiModelProperty("发送方用户的id")
    private Long userIdFrom;
    @ApiModelProperty("接收方用户的id")
    private Long userIdTo;
    @ApiModelProperty("消息内容")
    private String content;
    @ApiModelProperty("发送方消息状态,0:正常,1:删除")
    private Integer statusFrom;
    @ApiModelProperty("接收方消息状态,0:正常,1:删除")
    private Integer statusTo;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
