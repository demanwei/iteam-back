package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("it_friend")
@ApiModel("好友关系")
public class Friend {
    private Long id;
    @ApiModelProperty("用户的id")
    private Long userId;
    @ApiModelProperty("好友的id")
    private Long friendId;
    @ApiModelProperty("申请状态,0:拒绝,1:好友,2:已发送申请")
    private Integer apply;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
