package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@TableName("it_user")
@ApiModel("用户")
public class User {
    private Long id;
    @ApiModelProperty("用户名,登录的唯一凭证")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("地址")
    private String addr;
    @ApiModelProperty("性别")
    private String gender;
    @ApiModelProperty("头像的路径")
    private String headImg;
    @ApiModelProperty("经度")
    private Double lon;
    @ApiModelProperty("维度")
    private Double lat;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
