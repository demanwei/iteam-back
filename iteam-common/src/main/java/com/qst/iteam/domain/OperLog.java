package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("op_operlog")
@ApiModel("操作日志")
public class OperLog {
    private Long id;
    @ApiModelProperty("操作者的id")
    private Long runnerId;
    @ApiModelProperty("操作者类型,mobile|web")
    private String runnerType;
    @ApiModelProperty("访问的路径")
    private String path;
    @ApiModelProperty("携带的请求参数")
    private String param;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
