package com.qst.iteam.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("数据库信息")
public class DbInfo {
    @ApiModelProperty("数据库名称")
    private String databaseProductName;
    @ApiModelProperty("数据库版本")
    private String databaseProductVersion;
    @ApiModelProperty("驱动名称")
    private String driverName;
}
