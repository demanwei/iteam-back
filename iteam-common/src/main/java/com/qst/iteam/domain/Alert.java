package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("op_alert")
@ApiModel("预警")
public class Alert {
    private Long id;
    @ApiModelProperty("指标名称")
    private String metricName;
    @ApiModelProperty("指标真实值")
    private Double metricValueReal;
    @ApiModelProperty("指标规则阈值")
    private Double metricValueRule;
    @ApiModelProperty("指标单位")
    private String metricUnit;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
