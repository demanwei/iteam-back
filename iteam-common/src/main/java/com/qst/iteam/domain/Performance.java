package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("op_performance")
@ApiModel("性能指标")
public class Performance {
    private Long id;
    @ApiModelProperty("cpu使用率")
    private Double cpuUsage;
    @ApiModelProperty("cpu指标单位")
    private String cpuUnit;
    @ApiModelProperty("总内存")
    private Double memoryTotal;
    @ApiModelProperty("已使用内存")
    private Double memoryUsed;
    @ApiModelProperty("内存指标单位")
    private String memoryUnit;
    @ApiModelProperty("上行网络流量")
    private Double netRx;
    @ApiModelProperty("下行网络流量")
    private Double netTx;
    @ApiModelProperty("网络流量指标单位")
    private String netUnit;
    @ApiModelProperty("磁盘总空间")
    private Double diskTotal;
    @ApiModelProperty("磁盘已用空间")
    private Double diskUsed;
    @ApiModelProperty("磁盘指标单位")
    private String diskUnit;
    @ApiModelProperty("jvm已使用堆内存")
    private Double heapUsed;
    @ApiModelProperty("jvm最大堆内存")
    private Double heapMax;
    @ApiModelProperty("jvm堆内存指标单位")
    private String heapUnit;
    @ApiModelProperty("jvm已使用非堆内存")
    private Double nonHeapUsed;
    @ApiModelProperty("jvm最大非堆内存")
    private Double nonHeapMax;
    @ApiModelProperty("jvm非堆内存指标单位")
    private String nonHeapUnit;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
