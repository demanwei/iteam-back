package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("op_api")
@ApiModel("接口")
public class Api {
    private Long id;
    @ApiModelProperty("操作端,desktop|mobile|web")
    private String path;
    @ApiModelProperty("访问路径")
    private String end;
    @ApiModelProperty("接口描述")
    private String intro;
    @ApiModelProperty("请求方法")
    private String method;
    @ApiModelProperty("参数传递类型,query|path|body|form")
    private String paramType;
    @ApiModelProperty("参数值示例")
    private String paramExample;
    @ApiModelProperty("响应体")
    private String responseBody;
    @ApiModelProperty("访问次数")
    private Long visit;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
