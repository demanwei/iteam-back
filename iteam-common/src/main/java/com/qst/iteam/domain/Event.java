package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("it_event")
@ApiModel("活动")
public class Event {
    private Long id;
    @ApiModelProperty("哪个用户创建的,他的id")
    private Long userId;
    @ApiModelProperty("活动名称")
    private String name;
    @ApiModelProperty("活动简介")
    private String intro;
    @ApiModelProperty("活动地址")
    private String addr;
    @ApiModelProperty("活动海报图片的路径")
    private String headImg;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("活动开始时间")
    private LocalDateTime startTime;
    @ApiModelProperty("经度")
    private Double lon;
    @ApiModelProperty("维度")
    private Double lat;
    @ApiModelProperty("活动状态")
    private Integer state;
    @ApiModelProperty("活动是否已被删除")
    private Integer ifDelete;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
