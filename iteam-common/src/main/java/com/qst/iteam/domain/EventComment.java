package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("it_event_comment")
@ApiModel("活动评论")
public class EventComment {
    private Long id;
    @ApiModelProperty("评论的活动的id")
    private Long eventId;
    @ApiModelProperty("哪个用户评论的,他的id")
    private Long userId;
    @ApiModelProperty("评论内容")
    private String content;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
