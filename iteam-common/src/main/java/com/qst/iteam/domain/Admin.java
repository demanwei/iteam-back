package com.qst.iteam.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("op_admin")
@ApiModel("管理员")
public class Admin {
    private Long id;
    @ApiModelProperty("管理员用户名,登录的唯一凭证")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("操作端,desktop|web")
    private String end;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("是否接收邮件提醒, 1:是,0:否")
    private Integer receiveEmail;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
