package com.qst.iteam.dto;

import com.qst.iteam.domain.Event;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("活动(数据传输对象)")
public class EventDto extends Event {
    @ApiModelProperty("加入这个活动的状态,2:我发起,1:加入,0:未加入")
    private Integer join;
}
