package com.qst.iteam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("活动成员(数据传输对象)")
public class EventMemberDto {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("用户姓名")
    private String name;
    @ApiModelProperty("用户地址")
    private String addr;
    @ApiModelProperty("用户头像路径")
    private String headImg;
    @ApiModelProperty("该用户和我的关系,0:拒绝,1:好友,2:已发送申请")
    private Integer apply;
}
