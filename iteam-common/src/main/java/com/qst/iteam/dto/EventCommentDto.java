package com.qst.iteam.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("活动评论(数据传输对象)")
public class EventCommentDto {
    @ApiModelProperty("评论内容")
    private String content;
    @ApiModelProperty("哪个用户评论的,他的name")
    private String name;
    @ApiModelProperty("哪个用户评论的,他的头像")
    private String headImg;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
