package com.qst.iteam.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("活动小队(数据传输对象)")
public class TeamDto {
    @ApiModelProperty("活动的id")
    private Long eventId;
    @ApiModelProperty("活动名称")
    private String eventName;
    @ApiModelProperty("哪个用户创建的,他的id")
    private Long createUserId;
    @ApiModelProperty("用户名")
    private String userInfoName;
    @ApiModelProperty("参加活动的人数")
    private Integer memberCount;
    @ApiModelProperty("活动海报的路径")
    private String headImg;
    @ApiModelProperty("活动地址")
    private String addr;
}
