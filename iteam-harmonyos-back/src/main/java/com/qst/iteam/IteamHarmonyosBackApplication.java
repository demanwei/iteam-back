package com.qst.iteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy // 开启AOP自动代理
public class IteamHarmonyosBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(IteamHarmonyosBackApplication.class, args);
    }
}
