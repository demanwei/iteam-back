package com.qst.iteam.aspect;

import com.qst.iteam.domain.OperLog;
import com.qst.iteam.service.OperLogService;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.ReflectUtils;
import com.qst.iteam.utils.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Component
@Aspect
public class OperLogAspect {
    @Autowired
    private OperLogService operLogService;

    @Before("execution(* com.qst.iteam.controller.*.*(..)) && " +
            "!execution(* com.qst.iteam.controller.UserController.login(..)) && " +
            "!execution(* com.qst.iteam.controller.UserController.logout(..))")
    public void before(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();

        // 访问路径: controller的path + method的path
        String path = "";
        Method method = ((MethodSignature) signature).getMethod();
        Class<?> controllerClass = method.getDeclaringClass();
        RequestMapping controllerRequestMapping = controllerClass.getAnnotation(RequestMapping.class);
        String controllerPath = controllerRequestMapping.value()[0];
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (postMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(postMapping.value()[0]);
            // 保存操作日志
            saveOperLog(joinPoint, method, path);
        } else if (putMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(putMapping.value()[0]);
            // 保存操作日志
            saveOperLog(joinPoint, method, path);
        } else if (deleteMapping != null) {
            path = controllerPath + StringUtils.getCorrectPath(deleteMapping.value()[0]);
            // 保存操作日志
            saveOperLog(joinPoint, method, path);
        }
    }


    /**
     * 对于非查询操作, 记录操作日志
     */
    private void saveOperLog(JoinPoint joinPoint, Method method, String path) {
        OperLog operLog = new OperLog();
        // 操作者类型
        operLog.setRunnerType(Constants.END);
        // 访问path
        operLog.setPath(path);

        // 获取请求参数
        Object[] args = joinPoint.getArgs();
        Parameter[] parameters = method.getParameters();
        StringBuilder param = new StringBuilder();
        for (int i = 0; i < parameters.length; i++) {
            // 参数metadata
            Parameter parameter = parameters[i];
            // 参数名
            String parameterName = parameter.getName();
            // 参数值
            Object parameterValue = args[i];
            // 如果参数名为runnerId,且有@RequestHeader注解,且头的值不为null,则
            if ("runnerId".equals(parameterName) && parameter.getAnnotation(RequestHeader.class) != null
                    && parameterValue != null) {
                // 可以获取操作者id
                operLog.setRunnerId((long) parameterValue);
                // 跳过后面的,不要把header放到param里
                continue;
            }
            // 基本类型 => userId=1,
            if (ReflectUtils.isPrimitive(parameterValue)) {
                param.append(parameterName)
                        .append("=")
                        .append(parameterValue)
                        .append(",");
            } else if (parameterValue != null) {    // 调用时的参数可能没传,用的默认值
                // [Rule(id=null, metricName=cpu_usage, metricValue=10, metricUnit=percent, userId=1, createTime=null, updateTime=null)]
                // [User(id=null, username=90217, password=123abc, createTime=null)]
                // 引用类型 => username=90217, password=123abc,
                for (Field field : parameterValue.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        Object value = field.get(parameterValue);
                        if (value != null) {
                            param.append(field.getName())
                                    .append("=")
                                    .append(value)
                                    .append(",");
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // 没有参数,不设置,直接为null
        if (param.length() > 1) {
            // 去掉最后一个逗号","
            param.deleteCharAt(param.length() - 1);
            operLog.setParam(param.toString());
        }

        // 如果追踪到了userId,保存至数据库
        if (operLog.getRunnerId() != null) {
            operLogService.save(operLog);
        }
    }
}
