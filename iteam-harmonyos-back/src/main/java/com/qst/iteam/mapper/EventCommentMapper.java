package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.EventComment;
import com.qst.iteam.dto.EventCommentDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EventCommentMapper extends BaseMapper<EventComment> {
    List<EventCommentDto> listByEventId(Long eventId);
}
