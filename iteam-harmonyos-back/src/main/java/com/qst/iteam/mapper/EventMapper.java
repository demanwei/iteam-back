package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Event;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EventMapper extends BaseMapper<Event> {
    Event recommend();
    List<Event> nearbyEventList(Double maxLat, Double minLat, Double maxLont, Double minLont);
}
