package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.EventMember;
import com.qst.iteam.dto.EventMemberDto;
import com.qst.iteam.dto.TeamDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EventMemberMapper extends BaseMapper<EventMember> {
    List<EventMemberDto> eventMemberList(Long eventId, Long userId);
    List<TeamDto> teamList(Long userId);
}
