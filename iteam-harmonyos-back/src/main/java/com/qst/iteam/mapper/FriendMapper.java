package com.qst.iteam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qst.iteam.domain.Friend;
import com.qst.iteam.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FriendMapper extends BaseMapper<Friend> {
    List<User> getFriendList(Long userId);
    List<User> applyList(Long userId);
}
