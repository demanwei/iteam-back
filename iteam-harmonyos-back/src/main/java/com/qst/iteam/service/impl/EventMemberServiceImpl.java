package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.EventMember;
import com.qst.iteam.dto.EventMemberDto;
import com.qst.iteam.dto.TeamDto;
import com.qst.iteam.mapper.EventMemberMapper;
import com.qst.iteam.service.EventMemberService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventMemberServiceImpl extends ServiceImpl<EventMemberMapper, EventMember> implements EventMemberService {
    @Override
    public List<EventMemberDto> eventMemberList(Long eventId, Long userId) {
        return baseMapper.eventMemberList(eventId, userId);
    }

    @Override
    public List<TeamDto> teamList(Long userId) {
        return baseMapper.teamList(userId);
    }

}
