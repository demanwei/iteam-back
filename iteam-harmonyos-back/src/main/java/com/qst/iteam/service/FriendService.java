package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Friend;
import com.qst.iteam.domain.User;

import java.util.List;

public interface FriendService extends IService<Friend> {
    List<User> getFriendList(Long userId);
    List<User> applyList(Long userId);
}
