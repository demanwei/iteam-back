package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.Event;
import com.qst.iteam.mapper.EventMapper;
import com.qst.iteam.service.EventService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl extends ServiceImpl<EventMapper, Event> implements EventService {
    @Override
    public Event recommend() {
        return baseMapper.recommend();
    }

    @Override
    public List<Event> nearbyEventList(Double maxLat, Double minLat, Double maxLont, Double minLont) {
        return baseMapper.nearbyEventList(maxLat, minLat, maxLont, minLont);
    }
}
