package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.Api;
import com.qst.iteam.mapper.ApiMapper;
import com.qst.iteam.service.ApiService;
import org.springframework.stereotype.Service;

@Service
public class ApiServiceImpl extends ServiceImpl<ApiMapper, Api> implements ApiService {
}
