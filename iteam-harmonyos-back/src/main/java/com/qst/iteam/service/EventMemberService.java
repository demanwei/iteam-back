package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.EventMember;
import com.qst.iteam.dto.EventMemberDto;
import com.qst.iteam.dto.TeamDto;

import java.util.List;

public interface EventMemberService extends IService<EventMember> {
    List<EventMemberDto> eventMemberList(Long eventId, Long userId);
    List<TeamDto> teamList(Long userId);
}
