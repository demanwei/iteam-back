package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Event;

import java.util.List;

public interface EventService extends IService<Event> {
    Event recommend();

    List<Event> nearbyEventList(Double maxLat, Double minLat, Double maxLont, Double minLont);
}
