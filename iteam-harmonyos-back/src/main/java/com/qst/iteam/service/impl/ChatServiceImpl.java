package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.Chat;
import com.qst.iteam.mapper.ChatMapper;
import com.qst.iteam.service.ChatService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatServiceImpl extends ServiceImpl<ChatMapper, Chat> implements ChatService {
    @Override
    public List<Chat> byUserIdFromAndTo(Long userIdFrom, Long userIdTo) {
        return baseMapper.byUserIdFromAndTo(userIdFrom, userIdTo);
    }
}
