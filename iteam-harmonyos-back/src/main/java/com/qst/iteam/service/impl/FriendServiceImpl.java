package com.qst.iteam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qst.iteam.domain.Friend;
import com.qst.iteam.domain.User;
import com.qst.iteam.mapper.FriendMapper;
import com.qst.iteam.service.FriendService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend> implements FriendService {
    @Override
    public List<User> getFriendList(Long userId) {
        return baseMapper.getFriendList(userId);
    }

    @Override
    public List<User> applyList(Long userId) {
        return baseMapper.applyList(userId);
    }
}
