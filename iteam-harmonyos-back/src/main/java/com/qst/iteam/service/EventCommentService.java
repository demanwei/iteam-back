package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.EventComment;
import com.qst.iteam.dto.EventCommentDto;

import java.util.List;

public interface EventCommentService extends IService<EventComment> {
    List<EventCommentDto> listByEventId(Long eventId);
}
