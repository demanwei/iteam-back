package com.qst.iteam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qst.iteam.domain.Chat;

import java.util.List;

public interface ChatService extends IService<Chat> {
    List<Chat> byUserIdFromAndTo(Long userIdFrom, Long userIdTo);
}
