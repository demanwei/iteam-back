package com.qst.iteam.controller;

import com.qst.iteam.domain.Chat;
import com.qst.iteam.service.ChatService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/chat")
@Api(tags = "聊天相关接口")
public class ChatController {
    @Autowired
    private ChatService chatService;

    @GetMapping("/byUserIdFromAndTo")
    @ApiOperation("根据两个用户的id获取聊天记录列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userIdFrom", value = "用户1的id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "userIdTo", value = "用户2的id", required = true, paramType = "query", example = "2"),
    })
    public R<List<Chat>> byUserIdFromAndTo(Long userIdFrom, Long userIdTo) {
        if (userIdFrom == null || userIdTo == null) {
            return R.error("请求字段缺失");
        }
        List<Chat> chatList = chatService.byUserIdFromAndTo(userIdFrom, userIdTo);
        return R.success(chatList);
    }


    @PostMapping("/insert")
    @ApiOperation("新增聊天记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chat", value = "聊天记录chat实体", required = true, paramType = "body",
                    example = "{\"userIdFrom\": 1, \"userIdTo\": 2, \"content\":\"Hello!\"}")
    })
    public R<Object> insert(@RequestBody Chat chat,
                            @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (chat == null || chat.getUserIdFrom() == null
                || chat.getUserIdTo() == null || chat.getContent() == null) {
            return R.error("请求字段缺失");
        }
        // 初始化消息状态都为0
        chat.setStatusFrom(0);
        chat.setStatusTo(0);
        // 插入表
        boolean success = chatService.save(chat);
        return success ? R.success() : R.error();
    }


    @DeleteMapping("/delete")
    @ApiOperation("根据chatId删除聊天记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chatId", value = "chat的id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "userIdFrom", value = "如果是发送方删除消息,发送方的id,注意只能传一方", required = false, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "userIdTo", value = "如果是接收方删除消息,接收方的id,注意只能传一方", required = false, paramType = "query", example = "2"),
    })
    public R<Object> delete(Long chatId, Long userIdFrom, Long userIdTo,
                            @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (chatId == null) {
            return R.error("请求字段缺失");
        }
        // 创建实体
        Chat chat = chatService.getById(chatId);
        if (userIdFrom != null && userIdTo != null) {
            return R.error("请求字段有误");
        }
        if (userIdFrom != null) {
            // 如果是发送方删除消息
            if (!userIdFrom.equals(chat.getUserIdFrom())) {
                return R.error("此消息不是该用户所发");
            }
            chat.setStatusFrom(1);
        } else if (userIdTo != null) {
            // 如果是接收方删除消息
            if (!userIdTo.equals(chat.getUserIdTo())) {
                return R.error("此消息不是该用户所收");
            }
            chat.setStatusTo(1);
        }
        // 更新表
        boolean success = chatService.updateById(chat);
        return success ? R.success() : R.error();
    }
}
