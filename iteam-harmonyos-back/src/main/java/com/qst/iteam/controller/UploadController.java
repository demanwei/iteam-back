package com.qst.iteam.controller;

import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/upload")
@Api(tags = "文件上传接口")
public class UploadController {
    @Autowired
    private MultipartProperties multipartProperties;


    @ApiOperation("上传文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "上传的文件", required = true, paramType = "form")
    })
    @PostMapping("/uploadFile")
    public R<String> upload(MultipartFile file, HttpServletResponse response,
                            @RequestHeader(value = "userId", required = false) Long runnerId) throws IOException {
        if (file == null || file.getContentType() == null) {
            return R.error("请求字段缺失");
        }
        // 给文件重命名
        String fileName = UUID.randomUUID() + "." + file.getContentType()
                .substring(file.getContentType().lastIndexOf("/") + 1);
        fileName.replace("-", "_");
        // 获取保存路径
        String saveDir = multipartProperties.getLocation();
        File savePath = new File(saveDir, fileName).getAbsoluteFile();
        File parentDir = savePath.getParentFile();
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }
        file.transferTo(savePath);
        response.addHeader("Content-Disposition", "filename=" + "/upload/" + fileName);
        return R.success("/upload/" + fileName);
    }
}

