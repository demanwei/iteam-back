package com.qst.iteam.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.EventMember;
import com.qst.iteam.domain.Friend;
import com.qst.iteam.dto.EventMemberDto;
import com.qst.iteam.dto.TeamDto;
import com.qst.iteam.service.EventMemberService;
import com.qst.iteam.service.FriendService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/eventMember")
@Api(tags = "活动成员相关接口")
public class EventMemberController {
    @Autowired
    private EventMemberService eventMemberService;
    @Autowired
    private FriendService friendService;

    @PostMapping("/join")
    @ApiOperation("加入活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "form", example = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "form", example = "1"),
    })
    public R<Object> join(Long eventId, Long userId,
                          @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (eventId == null || userId == null) {
            return R.error("请求字段缺失");
        }
        // 查询是否已加入
        EventMember eventMember = eventMemberService.getOne(
                new LambdaQueryWrapper<EventMember>()
                        .eq(EventMember::getEventId, eventId)
                        .eq(EventMember::getUserId, userId), false);
        boolean success;
        // true:最终加入了(之前未加入), false:最终取消加入
        boolean endIsJoin;
        // 是否已经加入活动
        if (eventMember == null) {
            // 未加入,现在要加入
            // 构造EventMember对象
            eventMember = new EventMember();
            eventMember.setEventId(eventId);
            eventMember.setUserId(userId);
            success = eventMemberService.save(eventMember);
            endIsJoin = true;
        } else {
            // 已加入,现在要取消加入
            success = eventMemberService.removeById(eventMember.getId());
            endIsJoin = false;
        }
        return success ? R.success(endIsJoin) : R.error();
    }


    @GetMapping("/list")
    @ApiOperation("某个活动所有的成员列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1"),
    })
    public R<List<EventMemberDto>> eventMemberList(Long eventId, Long userId) {
        if (eventId == null || userId == null) {
            return R.error("请求字段缺失");
        }
        List<EventMemberDto> memberDtoList = eventMemberService.eventMemberList(eventId, userId);
        // 设置每个活动成员和我的关系
        memberDtoList.forEach((memberDto) -> {
            Friend friend = friendService.getOne(new LambdaQueryWrapper<Friend>()
                    .eq(Friend::getUserId, userId)
                    .eq(Friend::getFriendId, memberDto.getUserId())
            );
            if (friend != null) {
                memberDto.setApply(friend.getApply());
            }
        });
        return R.success(memberDtoList);
    }


    @GetMapping("/teamList")
    @ApiOperation("获取小队信息(我加入的所有活动列表)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1")
    })
    public R<List<TeamDto>> teamList(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // 获取teamDto
        List<TeamDto> teamDtoList = eventMemberService.teamList(userId);
        // teamDto设置参加人数,根据eventId查询event_member表统计人数
        teamDtoList.forEach((teamDto) -> {
            Long eventId = teamDto.getEventId();
            Integer memberCount = eventMemberService.count(
                    new LambdaQueryWrapper<EventMember>()
                            .eq(EventMember::getEventId, eventId)
            );
            // event_member表不存创建者,因此还要加上创建者作为总人数
            teamDto.setMemberCount(memberCount + 1);
        });
        return R.success(teamDtoList);
    }
}
