package com.qst.iteam.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Event;
import com.qst.iteam.domain.EventMember;
import com.qst.iteam.dto.EventDto;
import com.qst.iteam.service.EventMemberService;
import com.qst.iteam.service.EventService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/event")
@Api(tags = "活动相关接口")
public class EventController {
    @Autowired
    private EventService eventService;
    @Autowired
    private EventMemberService eventMemberService;


    @GetMapping("/info")
    @ApiOperation("根据eventId获取某个活动的信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query", example = "1")
    })
    public R<Event> info(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        Event event = eventService.getById(eventId);
        if (event == null) {
            return R.error("未找到该活动");
        }
        return R.success(event);
    }


    @GetMapping("/isJoin")
    @ApiOperation("判断某用户是否参加了某个活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1")
    })
    public R<Integer> isJoin(Long eventId, Long userId) {
        if (eventId == null || userId == null) {
            return R.error("请求字段缺失");
        }
        Event event = eventService.getOne(
                new LambdaQueryWrapper<Event>()
                        .eq(Event::getId, eventId)
                        .eq(Event::getUserId, userId), false);
        if (event != null) {
            // 由我发起的
            return R.success(2);
        }
        // 已加入但不是我发起的
        EventMember eventMember = eventMemberService.getOne(
                new LambdaQueryWrapper<EventMember>()
                        .eq(EventMember::getEventId, eventId)
                        .eq(EventMember::getUserId, userId), false);
        if (eventMember != null) {
            return R.success(1);
        }
        // 未加入
        return R.success(0);
    }


    @GetMapping("/list")
    @ApiOperation("获取活动列表|搜索活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "kw", value = "搜索关键字", required = false, paramType = "query", example = "骑行"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = false, paramType = "query", example = "1")
    })
    public R<List<? extends Event>> list(String kw, Long userId) {
        // 按照活动创建时间倒序排序
        LambdaQueryWrapper<Event> lambdaQueryWrapper = new LambdaQueryWrapper<Event>()
                .orderByDesc(Event::getCreateTime);
        if (kw != null) {
            // 如果传入kw,表示搜索活动,按照name和intro字段模糊匹配
            lambdaQueryWrapper
                    .like(Event::getName, kw)
                    .or()
                    .like(Event::getIntro, kw);
        }

        List<Event> eventList = eventService.list(lambdaQueryWrapper);

        // 不传userId,只查询活动信息
        if (userId == null) {
            return R.success(eventList);
        }
        // 传入userId,还要查询用户是否加入了活动
        List<EventDto> eventDtoList = eventList.stream()
                .map((event) -> {
                    EventDto eventDto = new EventDto();
                    // 复制event的所有属性到eventDto
                    BeanUtil.copyProperties(event, eventDto, true);
                    // 查询是否加入活动
                    int join;
                    if (userId.equals(event.getUserId())) {
                        // 2:由我发起的
                        join = 2;
                    } else {
                        EventMember eventMember = eventMemberService.getOne(
                                new LambdaQueryWrapper<EventMember>()
                                        .eq(EventMember::getEventId, event.getId())
                                        .eq(EventMember::getUserId, userId), false);
                        // 1:已加入但不是我发起的, 0:未加入
                        join = eventMember != null ? 1 : 0;
                    }
                    eventDto.setJoin(join);
                    return eventDto;
                }).collect(Collectors.toList());
        return R.success(eventDtoList);
    }


    @PostMapping("/insert")
    @ApiOperation("新增活动信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "event", value = "活动实体", required = true, paramType = "body",
                    example = "{\"name\": \"插入活动测试\", \"userId\": 1, \"intro\": \"活动内容....\"}")
    })
    public R<Event> insert(@RequestBody Event event,
                           @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (event == null) {
            return R.error("请求字段缺失");
        }
        // 插入表
        boolean success = eventService.save(event);
        return success ? R.success(event) : R.error();
    }

    @DeleteMapping("/delete")
    @ApiOperation("删除活动信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query", example = "1"),
    })
    public R<Object> delete(Long eventId,
                            @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        // 删除event表的记录
        boolean success = eventService.removeById(eventId);
        // 删除event_member表的所有相关记录
        eventMemberService.remove(new LambdaQueryWrapper<EventMember>()
                .eq(EventMember::getEventId, eventId));
        return success ? R.success() : R.error();
    }


    @GetMapping("/recommend")
    @ApiOperation("活动推荐")
    public R<Event> recommend() {
        Event event = eventService.recommend();
        return R.success(event);
    }


    @GetMapping("/nearbyList")
    @ApiOperation("获取附近活动列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "targetLat", value = "目标精度", required = true, paramType = "query", example = "36.1"),
            @ApiImplicitParam(name = "targetLont", value = "目标维度", required = true, paramType = "query", example = "120.4"),
            @ApiImplicitParam(name = "distance", value = "距离", required = true, paramType = "query", example = "1.2"),
    })
    public R<List<Event>> nearbyEventList(Double targetLat, Double targetLont, Double distance) {
        if (targetLat == null || targetLont == null || distance == null) {
            return R.error("请求字段缺失");
        }
        Double maxLat = targetLat + distance;
        Double minLat = targetLat - distance;
        Double maxLont = targetLont + distance;
        Double minLont = targetLont - distance;
        List<Event> eventList = eventService.nearbyEventList(maxLat, minLat, maxLont, minLont);
        return R.success(eventList);
    }
}
