package com.qst.iteam.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Friend;
import com.qst.iteam.domain.User;
import com.qst.iteam.service.FriendService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/friend")
@Api(tags = "好友相关接口")
public class FriendController {
    @Autowired
    private FriendService friendService;

    @GetMapping("/getFriendList")
    @ApiOperation("根据userId查询好友列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1")
    })
    public R<List<User>> getFriendList(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        List<User> friendList = friendService.getFriendList(userId);
        return R.success(friendList);
    }

    @PostMapping("/sendApply")
    @ApiOperation("发送好友申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "form", example = "1"),
            @ApiImplicitParam(name = "friendId", value = "对方用户id", required = true, paramType = "form", example = "2")
    })
    public R<Object> sendApply(Long userId, Long friendId,
                               @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (userId == null || friendId == null) {
            return R.error("请求字段缺失");
        }
        // 构造friend对象,插入
        Friend friend = new Friend();
        friend.setUserId(userId);
        friend.setFriendId(friendId);
        // apply=2表示发送申请
        friend.setApply(2);
        boolean success = friendService.save(friend);
        return success ? R.success() : R.error();
    }


    @PostMapping("/processApply")
    @ApiOperation("处理好友申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "form", example = "2"),
            @ApiImplicitParam(name = "friendId", value = "对方用户id", required = true, paramType = "form", example = "1"),
            @ApiImplicitParam(name = "apply", value = "对方用户id,0表示拒绝,1表示同意", required = true, paramType = "form", example = "1")
    })
    public R<Object> processApply(Long userId, Long friendId, Integer apply,
                                  @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (userId == null || friendId == null || apply == null) {
            return R.error("请求字段缺失");
        }
        // 构造friend对象,插入
        Friend friend = friendService.getOne(new LambdaQueryWrapper<Friend>()
                .eq(Friend::getUserId, friendId)
                .eq(Friend::getFriendId, userId)
        );
        if (friend == null) {
            return R.error("好友关系查找失败");
        }
        // apply=2表示发送申请
        friend.setApply(apply);
        boolean success = friendService.updateById(friend);
        return success ? R.success() : R.error();
    }


    @GetMapping("/applyList")
    @ApiOperation("根据userId查询申请列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1")
    })
    public R<List<User>> applyList(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // apply为0:拒绝,1:同意,2:发送申请
        List<User> friendList = friendService.applyList(userId);
        return R.success(friendList);
    }


    @GetMapping("/status")
    @ApiOperation("查询当前用户和活动成员的关系：好友，已发送好友申请，未发送好友申请，被拒绝")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query", example = "1"),
            @ApiImplicitParam(name = "memberId", value = "活动成员id", required = true, paramType = "query", example = "2"),
    })
    public R<Integer> status(Long userId, Long memberId) {
        if (userId == null || memberId == null) {
            return R.error("请求字段缺失");
        }
        // apply为0:拒绝,1:好友,2:已发送申请
        Friend friend = friendService.getOne(new LambdaQueryWrapper<Friend>()
                .eq(Friend::getUserId, userId)
                .eq(Friend::getFriendId, memberId)
        );
        if (friend == null) {
            return R.error("好友关系查找失败");
        }
        return R.success(friend.getApply());
    }
}
