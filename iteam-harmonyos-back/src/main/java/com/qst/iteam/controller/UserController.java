package com.qst.iteam.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.User;
import com.qst.iteam.service.UserService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@RestController
@RequestMapping("/user")
@Api(tags = "用户相关接口")
public class UserController {
    @Autowired
    private UserService userService;

    // 图片保存路径
    private static final String HEAD_SAVE_ROOT_DIRECTORY = "/upload/user/";
    // 允许上传图片的格式
    private static final String[] IMG_EXTENSIONS = {"jpg", "png", "jpeg", "gif", "bmp"};

    @PostMapping("/login")
    @ApiOperation("用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "请求体,须包含username和password两个字段", required = true, paramType = "body",
                    example = "{\"username\":\"aaa\", \"password\": \"aaa\"}")
    })
    public R<User> login(@RequestBody User user) {
        if (user == null || StrUtil.isEmpty(user.getUsername()) || StrUtil.isEmpty(user.getPassword())) {
            return R.error("请求字段缺失");
        }
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getUsername, user.getUsername())
                .eq(User::getPassword, user.getPassword());
        // 查找
        User targetUser = userService.getOne(queryWrapper);
        if (targetUser == null) {
            return R.error("登陆失败，用户名或密码错误");
        }
        return R.success(targetUser);
    }

    @PostMapping("/reg")
    @ApiOperation("用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户实体,至少包含username和password两个字段", required = true, paramType = "body",
                    example = "{\"username\":\"aaaaaa\", \"password\": \"aaaaaa\"}")
    })
    public R<User> register(@RequestBody User user) {
        if (user == null || StrUtil.isEmpty(user.getUsername()) || StrUtil.isEmpty(user.getPassword())) {
            return R.error("请求字段缺失");
        }

        // 查找
        User targetUser = userService.getOne(new LambdaQueryWrapper<User>()
                .eq(User::getUsername, user.getUsername()));
        if (targetUser != null) {
            return R.error("注册失败，用户名已经存在");
        }
        // 插入
        boolean success = userService.save(user);
        return success ? R.success(user) : R.error();
    }

    @PostMapping("/logout")
    @ApiOperation("用户退出登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "form", example = "3")
    })
    public R<Object> logout(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // 删除token
        User user = userService.getById(userId);
        if (user == null) {
            return R.error("未找到该用户");
        }
        return R.success(user);
    }

    @GetMapping("/info")
    @ApiOperation("根据userId查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "query",
                    example = "1")
    })
    public R<Object> info(Long userId) {
        if (userId == null) {
            return R.error("请求字段缺失");
        }
        // 查找用户
        User user = userService.getById(userId);
        if (user == null) {
            return R.error("未找到该用户");
        }
        return R.success(user);
    }

    @PutMapping("/update")
    @ApiOperation("更新用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户实体,须包含用户id和要更新的字段", required = true, paramType = "body",
                    example = "{\"id\": 2, \"username\":\"aaabbb\", \"password\": \"aaabbb\"}")
    })
    public R<Object> update(@RequestBody User user,
                            @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (user == null || user.getId() == null) {
            return R.error("请求字段缺失");
        }
        // 根据id更新用户
        boolean success = userService.updateById(user);
        return success ? R.success() : R.error();
    }

    @PostMapping("/uploadHeadImg")
    @ApiOperation("上传用户头像")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, paramType = "form", example = "1"),
            @ApiImplicitParam(name = "file", value = "上传的头像文件", required = true, paramType = "form", example = "org.springframework.web.multipart.MultipartFile")
    })
    public R<Object> uploadHeadImg(Long userId, MultipartFile file,
                                   @RequestHeader(value = "userId", required = false) Long runnerId) throws IOException {
        if (userId == null || file == null) {
            return R.error("请求字段缺失");
        }
        // 查找用户是否存在
        User user = userService.getById(userId);
        if (user == null) {
            return R.error("未找到该用户");
        }
        // 判断文件后缀
        if (!Arrays.asList(IMG_EXTENSIONS)
                .contains(FileUtil.extName(file.getOriginalFilename()))) {
            return R.error("文件格式错误");
        }
        // 保存文件
        String loc = HEAD_SAVE_ROOT_DIRECTORY;
        File f = new File(loc);
        if (!f.exists()) {
            f.mkdirs();
        }
        String savePath = loc + user + file.getOriginalFilename();
        file.transferTo(new File(savePath));
        // 更新user表字段
        user.setHeadImg(savePath);
        boolean success = userService.updateById(user);
        return success ? R.success() : R.error();
    }
}
