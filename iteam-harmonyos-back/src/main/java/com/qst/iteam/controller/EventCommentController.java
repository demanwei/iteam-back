package com.qst.iteam.controller;

import com.qst.iteam.domain.EventComment;
import com.qst.iteam.dto.EventCommentDto;
import com.qst.iteam.service.EventCommentService;
import com.qst.iteam.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/eventComment")
@Api(tags = "活动评论相关接口")
public class EventCommentController {
    @Autowired
    private EventCommentService eventCommentService;

    @GetMapping("/listByEventId")
    @ApiOperation("根据eventId查找所有相关评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventId", value = "活动id", required = true, paramType = "query", example = "1")
    })
    public R<List<EventCommentDto>> listByEventId(Long eventId) {
        if (eventId == null) {
            return R.error("请求字段缺失");
        }
        // 查询列表
        List<EventCommentDto> eventCommentDtoList = eventCommentService.listByEventId(eventId);
        return R.success(eventCommentDtoList);
    }


    @PostMapping("/insert")
    @ApiOperation("新增评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "eventComment", value = "评论实体,须包含eventId,userId,content字段", required = true, paramType = "body",
                    example = "{\"eventId\": 1, \"userId\": 1, \"content\": \"Hahahahaha\"}")
    })
    public R<Object> insert(@RequestBody EventComment eventComment,
                            @RequestHeader(value = "userId", required = false) Long runnerId) {
        if (eventComment == null || eventComment.getEventId() == null
                || eventComment.getUserId() == null || eventComment.getContent() == null) {
            return R.error("请求字段缺失");
        }
        // 新增实体
        boolean success = eventCommentService.save(eventComment);
        return success ? R.success() : R.error();
    }
}
