package com.qst.iteam.handler;


import com.qst.iteam.utils.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 全局异常处理器, 捕获Controller方法中的异常
 **/
@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
public class GlobalExceptionHandler {
    /**
     * 捕获什么类型的异常
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> sqlIntegrityConstraintViolationExceptionHandler(Exception e) {
        e.printStackTrace();
        String message;
        // 如果含有"Duplicate entry",说明是违反了唯一约束
        String errMessage = e.getMessage();
        if (errMessage.contains("Duplicate entry")) {
            message = errMessage.split(" ")[2] + " 已存在,请尝试其他名称";
            return R.error(message);
        }
        if (errMessage.contains("foreign key constraint")) {
            message = "操作时出现外键约束错误,请确保操作值的合法性";
            return R.error(message);
        }
        return R.error("遇到未知错误,数据操作失败");
    }

    @ExceptionHandler(SQLException.class)
    public R<String> sqlExceptionHandler(Exception e) {
        e.printStackTrace();
        String message = e.getMessage();
        // 缺失非null值
        Pattern pattern = Pattern.compile("Field '(.*?)' doesn't have a default value");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            String target = matcher.group(1);
            return R.error("字段 [" + target + "] 缺少默认值");
        }
        return R.error("SQL执行异常: " + message);
    }

    @ExceptionHandler(IOException.class)
    public R<String> ioExceptionHandler(Exception e) {
        e.printStackTrace();
        return R.error("IO异常: " + e.getMessage());
    }
}
