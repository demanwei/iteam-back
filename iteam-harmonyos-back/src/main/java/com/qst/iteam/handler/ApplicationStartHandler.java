package com.qst.iteam.handler;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qst.iteam.domain.Api;
import com.qst.iteam.service.ApiService;
import com.qst.iteam.utils.Constants;
import com.qst.iteam.utils.StringUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
/** 生命周期执行相关逻辑 **/
public class ApplicationStartHandler {
    @Autowired
    private ApiService apiService;

    /**
     * 在SpringBoot应用启动后,同步api接口数据
     */
    @PostConstruct
    public void scanAndSyncApi() throws ClassNotFoundException {
        // 构建要插入或更新的api列表
        List<Api> apiInsertOrUpdateList = new ArrayList<>();

        // 获取com.qst.iteam.controller包下的所有Class
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Controller.class));
        Set<BeanDefinition> candidates = scanner.findCandidateComponents("com.qst.iteam.controller");

        for (BeanDefinition candidate : candidates) {
            Class<?> controllerClass = Class.forName(candidate.getBeanClassName());
            // 获取访问path
            RequestMapping controllerRequestMapping = controllerClass.getAnnotation(RequestMapping.class);
            String controllerPath = controllerRequestMapping.value()[0];

            // 所有的Method
            for (Method method : controllerClass.getDeclaredMethods()) {
                // 筛选出含有@XxxMapping的某个方法
                String[] pathAndRequestMethod = getPathAndRequestMethod(method);
                if (pathAndRequestMethod[0] != null && pathAndRequestMethod[1] != null) {
                    // 只有method有@XxxMapping才视作路由
                    String path = controllerPath + pathAndRequestMethod[0];
                    String requestMethod = pathAndRequestMethod[1];
                    // 根据end和path查询api
                    Api api = apiService.getOne(
                            new LambdaQueryWrapper<Api>()
                                    .eq(Api::getEnd, Constants.END)
                                    .eq(Api::getPath, path)
                    );
                    // 设请求参数信息
                    String[] paramTypeAndExample = getParamTypeAndExample(method);
                    String responseBody = getResponseBody(method);
                    String intro = getIntro(method);
                    // 新增接口信息
                    if (api == null) {
                        api = new Api();
                        api.setEnd(Constants.END);
                        api.setPath(path);
                        api.setMethod(requestMethod);
                        api.setParamType(paramTypeAndExample[0]);
                        api.setParamExample(paramTypeAndExample[1]);
                        // 获取返回值的类型
                        api.setResponseBody(responseBody);
                        // 获取接口描述
                        api.setIntro(intro);
                        // 初始化访问次数
                        api.setVisit(0L);
                        // 插入
                        apiInsertOrUpdateList.add(api);
                    } else {
                        boolean shouldUpdate = false;
                        // 验证数据库和项目的是否一致,不一致则更新为项目的
                        if (!Objects.equals(requestMethod, api.getMethod())) {
                            api.setMethod(requestMethod);
                            shouldUpdate = true;
                        }
                        if (!Objects.equals(paramTypeAndExample[0], api.getParamType())) {
                            api.setParamType(paramTypeAndExample[0]);
                            shouldUpdate = true;
                        }
                        if (!Objects.equals(paramTypeAndExample[1], api.getParamExample())) {
                            api.setParamExample(paramTypeAndExample[1]);
                            shouldUpdate = true;
                        }
                        if (!Objects.equals(responseBody, api.getResponseBody())) {
                            api.setResponseBody(responseBody);
                            shouldUpdate = true;
                        }
                        if (!Objects.equals(intro, api.getIntro())) {
                            api.setIntro(intro);
                            shouldUpdate = true;
                        }
                        // 修改
                        if (shouldUpdate) {
                            apiInsertOrUpdateList.add(api);
                        }
                    }
                }
            }
        }

        // 如果列表有数据,则执行插入或更新
        if (apiInsertOrUpdateList.size() > 0) {
            apiService.saveOrUpdateBatch(apiInsertOrUpdateList);
        }
    }

    private String[] getPathAndRequestMethod(Method method) {
        String path = null;
        String requestMethod = null;

        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (getMapping != null) {
            path = StringUtils.getCorrectPath(getMapping.value()[0]);
            requestMethod = "GET";
        } else if (postMapping != null) {
            path = StringUtils.getCorrectPath(postMapping.value()[0]);
            requestMethod = "POST";
        } else if (putMapping != null) {
            path = StringUtils.getCorrectPath(putMapping.value()[0]);
            requestMethod = "PUT";
        } else if (deleteMapping != null) {
            path = StringUtils.getCorrectPath(deleteMapping.value()[0]);
            requestMethod = "DELETE";
        }

        return new String[]{path, requestMethod};
    }

    private String[] getParamTypeAndExample(Method method) {
        String paramType = null;
        String paramExample = null;
        // 获取方法上的@ApiImplicitParams注解
        ApiImplicitParams apiImplicitParams = method.getAnnotation(ApiImplicitParams.class);
        if (apiImplicitParams != null) {
            StringBuilder paramTypeBuilder = new StringBuilder();
            StringBuilder exampleBuilder = new StringBuilder();
            ApiImplicitParam[] apiImplicitParamsArr = apiImplicitParams.value();
            // 遍历@ApiImplicitParam注解数组
            for (int i = 0; i < apiImplicitParamsArr.length; i++) {
                ApiImplicitParam apiImplicitParam = apiImplicitParamsArr[i];
                // 参数类型
                String type = apiImplicitParam.paramType();
                paramTypeBuilder.append(type);
                if (i < apiImplicitParamsArr.length - 1) {
                    paramTypeBuilder.append(",");
                }
                // 参数值示例
                if ("path".equals(type)) {
                    // 如果是path参数,则拼接/v
                    exampleBuilder.append("/")
                            .append(apiImplicitParam.example());
                } else if ("query".equals(type) || "form".equals(type)) {
                    // 如果是query|form参数,则拼接?k1=v1&k2=v2...
                    if (i == 0) {   // 第一个kv对前加?
                        exampleBuilder.append("?");
                    }
                    String k = apiImplicitParam.name();
                    String v = apiImplicitParam.example();
                    exampleBuilder.append(k)
                            .append("=")
                            .append(v);
                    if (i < apiImplicitParamsArr.length - 1) {  // 除最后一个kv对,前边的都要在最后加&
                        exampleBuilder.append("&");
                    }
                } else if ("body".equals(type)) {
                    // 如果是body参数,直接就是example
                    exampleBuilder.append(apiImplicitParam.example());
                }
            }
            paramType = paramTypeBuilder.length() > 0 ? paramTypeBuilder.toString() : null;
            paramExample = exampleBuilder.length() > 0 ? exampleBuilder.toString() : null;
        }
        return new String[]{paramType, paramExample};
    }


    private String getResponseBody(Method method) {
        Type type = method.getGenericReturnType();// 返回属性声明的Type类型
        if (type instanceof ParameterizedType) {
            //强转为ParameterizedType对象
            ParameterizedType parameterizedType = (ParameterizedType) type;
            //获取原始类型
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            String respType = actualTypeArguments[0].getTypeName();
            return "success=boolean, info=string, obj=" + respType;
        }
        return null;
    }


    private String getIntro(Method method) {
        ApiOperation apiOperationAnno = method.getAnnotation(ApiOperation.class);   // ApiOperation注解
        if (apiOperationAnno != null) {
            return apiOperationAnno.value();
        }
        return null;
    }
}
